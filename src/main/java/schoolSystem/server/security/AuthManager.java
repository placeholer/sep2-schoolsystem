package schoolSystem.server.security;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import schoolSystem.server.dao.AbstractDAOFactory;
import schoolSystem.shared.domain.Password;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.security.InvalidPasswordException;
import schoolSystem.shared.security.InvalidTokenException;
import schoolSystem.shared.security.InvalidUsernameException;

public class AuthManager {
	private HashMap<UUID, User> loggedInUsers;
	private AbstractDAOFactory daoFactory;

	private static final Logger LOG = Logger.getLogger(AuthManager.class.getName());

	/**
	 * Initializes AuthManager with given DAO factory
	 *
	 * @param daoFactory	DAO factory
	 */
	public AuthManager(AbstractDAOFactory daoFactory) {
		LOG.info("Initializing AuthManager...");

		loggedInUsers = new HashMap<>();
		this.daoFactory = daoFactory;
	}

	/**
	 * Try to log in the user using given username and password
	 *
	 * @param username	username of the user
	 * @param password	password to use
	 * @return authentication token if login was successful
	 */
	public User login(String username, String password) throws InvalidUsernameException, InvalidPasswordException {
		LOG.info(String.format("Trying to log in user [%s]", username));

		List<Student> students = daoFactory.getStudentDAO().getByName(username);
		for(Student st: students) {
			Password p = daoFactory.getPasswordDAO().getForUser(st);
			if(p.auth(password))
				return login(st);
		}

		List<Teacher> teachers = daoFactory.getTeacherDAO().getByName(username);
		for(Teacher t: teachers) {
			Password p = daoFactory.getPasswordDAO().getForUser(t);
			if(p.auth(password))
				return login(t);
		}

		if(students.size() > 0 || teachers.size() > 0) {
			LOG.info(String.format("Invalid password for user [%s]", username));
			throw new InvalidPasswordException();
		}

		LOG.info(String.format("User [%s] does not exists", username));
		throw new InvalidUsernameException("Invalid credentials");
	}

	private User login(User u) {
		LOG.info(String.format("Logging the user [%s] in", u.getName()));

		UUID uuid = UUID.randomUUID();
		u.setAuthToken(uuid);
		loggedInUsers.put(uuid, u);

		return u;
	}

	/**
	 * Validates given token
	 *
	 * @param token	token to validate
	 * @return true if token is valid
	 */
	public boolean validate(UUID token) {
		return loggedInUsers.containsKey(token);
	}

	/**
	 * Gets User with given authentication token
	 * 
	 * @param token	User's authentication token
	 * @return User
	 */
	public User getUser(UUID token) {
		return loggedInUsers.get(token);
	}

	/**
	 * Logs out the User with given token
	 *
	 * @param authToken	User's token
	 */
	public void logout(UUID authToken) throws InvalidTokenException {
		User u = loggedInUsers.remove(authToken);
		if(null == u)
			throw new InvalidTokenException();

		LOG.info(String.format("Logging the user [%s] out", u.getName()));

		u.setAuthToken(null);
	}
}
