package schoolSystem.server;

/**
 * Base interface for SchoolSystemServer
 */
public interface SchoolSystemServer {

	/**
	 * Starts the server
	 */
	public void start();

	/**
	 * Stops the server
	 */
	public void close();
}
