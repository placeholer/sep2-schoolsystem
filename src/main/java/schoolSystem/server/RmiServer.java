package schoolSystem.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

import schoolSystem.shared.security.InvalidUsernameException;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.ObservableRemoteMediatorAdapter;
import schoolSystem.shared.security.InvalidPasswordException;
import schoolSystem.shared.security.InvalidTokenException;

/**
 * Base interface for RMI-based server
 */
public interface RmiServer extends Remote {

	/**
	 * Gets SchoolSystemMediator mediator
	 *
	 * @param authToken	authentication token
	 * @return SchoolSystemMediator instance
	 */
	public ObservableRemoteMediatorAdapter getMediator(UUID authToken) throws RemoteException, InvalidTokenException;

	/**
	 * Tries to log User in using given username and password
	 *
	 * @param username	the username
	 * @param password	the password
	 * @return authentication token
	 */
	public User login(String username, String password) throws RemoteException, InvalidUsernameException, InvalidPasswordException;

	/**
	 * Logs out the User with given authentication token
	 *
	 * @param authToken	token of the User to log out
	 */
	public void logout(UUID authToken) throws RemoteException, InvalidTokenException;

	/**
	 * Checks if authentication token is valid
	 * 
	 * @param token	authorization token
	 * @return true if token is valid
	 */
	public boolean validateToken(UUID token) throws RemoteException;

	/**
	 * Allows clients to ping the server
	 */
	public void ping() throws RemoteException;
}
