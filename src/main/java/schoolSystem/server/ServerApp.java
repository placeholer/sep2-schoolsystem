package schoolSystem.server;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.server.dao.hibernate.HibernateDAOFactory;
import schoolSystem.server.security.AuthManager;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.mediator.SchoolSystemMediator;
import schoolSystem.shared.mediator.SchoolSystemMediatorImpl;

public class ServerApp {

	private HibernateDAOFactory factory;
	private SchoolSystemServer server;
	private SchoolSystemMediator mediator;

	public ServerApp() {
		factory = DAOFactoryProducer.newHibernateDAOFactory();
		mediator = new SchoolSystemMediatorImpl(factory);

		server = new RmiServerImpl(mediator, new AuthManager(factory));
	}

	public void start() {
		server.start();
	}

	public void shutdown() {
		server.close();
		factory.close();

		System.exit(0);
	}

	public void createTestData() {
		System.out.println("################################################");
		System.out.println("CREATING TEST DATA");
		System.out.println("################################################");
		Student st = mediator.addStudent("test", "test");
		mediator.addStudent("Peter Lucas", "test");
		Teacher t = mediator.addTeacher("Bob the Builder", "server password");
		String desc = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod" +
			"tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At" +
			"vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren," +
			"no sea takimata sanctus est Lorem ipsum dolor sit amet.";
		Course c = mediator.addCourse("Databases", desc, Arrays.asList(t), Arrays.asList(st));
		Lecture l = mediator.addLecture(c, t, "Normalization", "Normalization of databases", LocalDate.now());
		mediator.addCourse("Java", "some more random descriptions", new ArrayList<>(), Arrays.asList(st));
	}
}
