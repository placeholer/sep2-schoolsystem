package schoolSystem.server;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import schoolSystem.server.security.AuthManager;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.ObservableRemoteMediatorAdapter;
import schoolSystem.shared.mediator.RemoteSchoolSystemMediatorImpl;
import schoolSystem.shared.mediator.SchoolSystemMediator;
import schoolSystem.shared.security.InvalidPasswordException;
import schoolSystem.shared.security.InvalidTokenException;
import schoolSystem.shared.security.InvalidUsernameException;

/**
 * Implementation of RMI-based SchoolSystemServer
 */
public class RmiServerImpl implements RmiServer, SchoolSystemServer {

	/**
	 * Name of the server for lookup
	 */
	private static final String NAME = "SchoolSystemServer";

	private Registry registry;
	private AuthManager authManager;
	private ObservableRemoteMediatorAdapter mediator;
	private ObservableRemoteMediatorAdapter mediatorStub;

	private static final Logger LOG = Logger.getLogger(RmiServerImpl.class.getName());

	/**
	 * Initializes RmiServerImpl with given mediator
	 *
	 * @param mediator	{@link schoolSystem.shared.mediator.SchoolSystemMediator} to export
	 */
	public RmiServerImpl(SchoolSystemMediator mediator, AuthManager authManager) {
		this.mediator = new RemoteSchoolSystemMediatorImpl(mediator, authManager);
		this.authManager = authManager;
	}

	/**
	 * Creates Registry and exports server
	 */
	@Override
	public void start() {
		LOG.info("Starting the server...");

		try {
			mediatorStub = (ObservableRemoteMediatorAdapter) UnicastRemoteObject.exportObject(mediator, 0);
			Remote stub = UnicastRemoteObject.exportObject(this, 0);

			LOG.info("Creating registry...");

			registry = LocateRegistry.createRegistry(1099);
			registry.rebind(NAME, stub);

			LOG.info("Server is up");
		} catch (RemoteException e) {
			LOG.log(Level.SEVERE, "Failed to start server", e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public ObservableRemoteMediatorAdapter getMediator(UUID token) throws RemoteException, InvalidTokenException {
		if(!authManager.validate(token))
			throw new InvalidTokenException();
		return mediatorStub;
	}

	@Override
	public User login(String username, String password) throws InvalidUsernameException, InvalidPasswordException {
		return authManager.login(username, password);
	}

	@Override
	public boolean validateToken(UUID token) {
		return authManager.validate(token);
	}

	@Override
	public void logout(UUID token) throws InvalidTokenException {
		authManager.logout(token);
	}

	/**
	 * Unexports registry and server
	 */
	@Override
	public void close() {
		LOG.info("Closing server...");
		if(null != registry){
			try {
				UnicastRemoteObject.unexportObject(registry, true);
				registry.unbind(NAME);
				UnicastRemoteObject.unexportObject(this, true);

			} catch (RemoteException | NotBoundException e) {
				LOG.log(Level.SEVERE, "Failed to stop the server", e);

				throw new RuntimeException(e);
			}
		}
		LOG.info("Server closed.");
	}

	@Override
	public void ping() {
		LOG.info("Ping request");
	}
}
