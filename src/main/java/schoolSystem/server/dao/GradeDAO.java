package schoolSystem.server.dao;

import java.util.List;

import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Grade;
import schoolSystem.shared.domain.Student;

/**
 * Base interface for Grade DAO's
 */
public interface GradeDAO extends DAO<Grade> {

	/**
	 * Gets Grades received by a Student
	 *
	 * @param student	the Student
	 * @return list of Grades received by a Student
	 */
	public List<Grade> getByStudent(Student student);

	/**
	 * Gets list of Grades for a Course
	 *
	 * @param course	the Course
	 * @return list of Grades
	 */
	public List<Grade> getByCourse(Course course);

	/**
	 * Gets Grades received by a Student for a Course
	 *
	 * @param student	the Student
	 * @param course	the Course
	 * @return list of Grades
	 */
	public List<Grade> getForStudentByCourse(Student student, Course course);
}
