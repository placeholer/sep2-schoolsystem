package schoolSystem.server.dao;

import java.util.List;

/**
 * Base interface for Data Access Object classes
 */
public interface DAO<T> {

	/**
	 * Get object by ID
	 *
	 * @param id	ID of an object
	 * @return Object with given ID
	 */
	public T get(Long id);

	/**
	 * Get all persisted objects
	 *
	 * @return List of all persisted objects
	 */
	public List<T> getAll();

	/**
	 * Persist an object
	 *
	 * @param obj	An object to persist
	 */
	public void persist(T obj);

	/**
	 * Delete an object
	 *
	 * @param obj	An object to delete
	 */
	public void delete(T obj);

	/**
	 * Update an object
	 *
	 * @param obj	An object to update
	 */
	public void update(T obj);
}
