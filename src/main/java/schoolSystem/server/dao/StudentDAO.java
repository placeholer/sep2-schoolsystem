package schoolSystem.server.dao;

import java.util.List;

import schoolSystem.shared.domain.AccountState;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.search.SearchProvider;

/**
 * Base interface for Student DAO's
 */
public interface StudentDAO extends DAO<Student>, SearchProvider<Student, String> {

	/**
	 * Gets list of students by name
	 *
	 * @param name	name of the Student
	 * @return list of Students
	 */
	public List<Student> getByName(String name);

	/**
	 * Gets list of Students with ACTIVE account
	 *
	 * @return list of Students
	 */
	public List<Student> getActive();

	/**
	 * Updates the account state of a User
	 *
	 * @param id	ID of the Student
	 * @param state	AccountState to set
	 */
	public void setAccountState(Long id, AccountState state);
}
