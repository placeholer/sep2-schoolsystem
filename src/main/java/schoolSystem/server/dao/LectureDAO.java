package schoolSystem.server.dao;

import java.time.LocalDate;
import java.util.List;

import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchProvider;

/**
 * Base interface for Lecture DAO's
 */
public interface LectureDAO extends DAO<Lecture>, SearchProvider<Lecture, String> {

	/**
	 * Gets Lectures for a given Course
	 *
	 * @param course	the Course
	 * @return list of Lectures for a Course
	 */
	public List<Lecture> getByCourse(Course course);

	/**
	 * Gets Lectures planned for a given date
	 *
	 * @param date	date the Lectures are planned for
	 * @return list of Lectures
	 */
	public List<Lecture> getByDate(LocalDate date);

	/**
	 * Gets Lectures planned in between given dates
	 *
	 * @param from	beginning of the period
	 * @param to	end of the period
	 * @return list of Lectures
	 */
	public List<Lecture> getInRange(LocalDate from, LocalDate to);

	/**
	 * Gets Lectures given by a Teacher
	 *
	 * @param teacher	the Teacher
	 * @return list of Lectures
	 */
	public List<Lecture> getByTeacher(Teacher teacher);

	/**
	 * Gets Lectures for a Student
	 *
	 * @param 	st	the Student
	 * @return list of Lectures
	 */
	public List<Lecture> getForStudent(Student st);

	/**
	 * Gets Lectures for a given Course the Student attends
	 *
	 * @param c	the Course
	 * @param st	the Student
	 * @return list of Lectures
	 */
	public List<Lecture> getByCourseForStudent(Course c, Student st);

	/**
	 * Gets Lectures for a Student planned between the dates (both inclusive)
	 *
	 * @param 	st	the Student
	 * @param from	beginning of the date range
	 * @param to	end of the date range
	 * @return list of Lectures
	 */
	public List<Lecture> getForStudent(Student st, LocalDate from, LocalDate to);

	public List<Lecture> getForUserByDate(User u, LocalDate date);
}
