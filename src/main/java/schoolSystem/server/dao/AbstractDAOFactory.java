package schoolSystem.server.dao;

/**
 * AbstractFactory interface for DAO's
 */
public interface AbstractDAOFactory {

	/**
	 * Gets StudentDAO
	 */
	public StudentDAO getStudentDAO();

	/**
	 * Gets TeacherDAO
	 */
	public TeacherDAO getTeacherDAO();

	/**
	 * Gets GradeDAO
	 */
	public GradeDAO getGradeDAO();

	/**
	 * Gets AttendanceDAO
	 */
	public AttendanceDAO getAttendanceDAO();

	/**
	 * Gets CourseDAO
	 */
	public CourseDAO getCourseDAO();

	/**
	 * Gets LectureDAO
	 */
	public LectureDAO getLectureDAO();

	/**
	 * Gets PasswordDAO
	 */
	public PasswordDAO getPasswordDAO();
}
