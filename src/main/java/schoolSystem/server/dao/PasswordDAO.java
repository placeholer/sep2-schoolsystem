package schoolSystem.server.dao;

import schoolSystem.shared.domain.Password;
import schoolSystem.shared.domain.User;

public interface PasswordDAO extends DAO<Password> {

	/**
	 * Gets Password assiciated with given user
	 *
	 * @param u	User whose Password to fetch
	 * @return Password associated with given user
	 */
	public Password getForUser(User u);

	/**
	 * Deletes a password for given User
	 *
	 * @param u	User to delete the Password for
	 */
	public void deleteForUser(User u);

	/**
	 * Updates password for User
	 * 
	 * @param u	User to update Password for
	 * @param p	new Password
	 */
	public void updateForUser(User u, Password p);
}
