package schoolSystem.server.dao.hibernate;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import schoolSystem.server.dao.LectureDAO;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.StringComparator;

public class HibernateLectureDAO extends AbstractHibernateDAO<Lecture> implements LectureDAO {

	public HibernateLectureDAO(SessionFactory sf) {
		super(sf, Lecture.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Lecture> getByCourse(Course course) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT l FROM Lecture l WHERE l.course.id = :id");
			q.setParameter("id", course.getId());

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Lecture> getByDate(LocalDate date) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT l FROM Lecture l WHERE l.date = :date");
			q.setParameter("date", date);

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Lecture> getInRange(LocalDate from, LocalDate to) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT l FROM Lecture l WHERE l.date BETWEEN :from AND :to");
			q.setParameter("from", from);
			q.setParameter("to", to);

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Lecture> getByTeacher(Teacher teacher) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT l FROM Lecture l WHERE l.teacher.id = :id");
			q.setParameter("id", teacher.getId());

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Lecture> getForStudent(Student st) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT l FROM Lecture l WHERE :student MEMBER OF l.course.students");
			q.setParameter("student", st);

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Lecture> getByCourseForStudent(Course c, Student st) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT l FROM Lecture l WHERE l.course.id = :course AND :student MEMBER OF l.course.students");
			q.setParameter("course", c.getId());
			q.setParameter("student", st);

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Lecture> getForStudent(Student st, LocalDate from, LocalDate to) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT l FROM Lecture l WHERE l.date BETWEEN :from AND :to AND :student MEMBER OF l.course.students");
			q.setParameter("from", from);
			q.setParameter("to", to);
			q.setParameter("student", st);

			return q.getResultList();
		}
	}

	@Override
	public List<SearchItem<Lecture>> search(String key) {
		try(Session s = getSession()) {
			Stream<Lecture> lectures = s.createQuery("SELECT l FROM Lecture l", Lecture.class).stream();
			StringComparator sc = new StringComparator();
			return lectures
				.map(l -> new SearchItem<Lecture>(l, sc.compare(key, l.getSubject())))
				.filter(i -> i.score >= 0)
				.sorted()
				.collect(Collectors.toList());
		}
	}

	@Override
	public List<Lecture> getForUserByDate(User u, LocalDate date) {
		try(Session s = getSession()) {
			TypedQuery<Lecture> q = s.createQuery(
					"SELECT l FROM Lecture l WHERE (:user MEMBER OF l.course.students OR :user MEMBER OF l.course.teachers) AND l.date = :date",
					Lecture.class);
			q.setParameter("user", u);
			q.setParameter("date", date);

			return q.getResultList();
		}
	}
}
