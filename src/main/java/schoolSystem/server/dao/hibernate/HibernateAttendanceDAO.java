package schoolSystem.server.dao.hibernate;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import schoolSystem.server.dao.AttendanceDAO;
import schoolSystem.shared.domain.Attendance;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;

public class HibernateAttendanceDAO extends AbstractHibernateDAO<Attendance> implements AttendanceDAO {

	public HibernateAttendanceDAO(SessionFactory sf) {
		super(sf, Attendance.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Attendance> getByStudent(Student st) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT a FROM Attendance a WHERE a.student.id = :id");
			q.setParameter("id", st.getId());

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Attendance> getByCourse(Course course) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT a FROM Attendance a WHERE a.lecture.course.id = :id");
			q.setParameter("id", course.getId());

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Attendance> getByLecture(Lecture lecture) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT a FROM Attendance a WHERE a.lecture.id = :id");
			q.setParameter("id", lecture.getId());

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Attendance> getForStudentByDate(Student st, LocalDate date) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT a FROM Attendance a WHERE a.student.id = :studentId AND a.lecture.date = :date");
			q.setParameter("studentId", st.getId());
			q.setParameter("date", date);

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Attendance> getForStudentByCourse(Student st, Course course) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT a FROM Attendance a WHERE a.student.id = :studentId AND a.lecture.course.id = :courseId");
			q.setParameter("studentId", st.getId());
			q.setParameter("courseId", course.getId());

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Attendance> getForStudentByLecture(Student st, Lecture lecture) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT a FROM Attendance a WHERE a.student.id = :studentId AND a.lecture.id = :lectureId");
			q.setParameter("studentId", st.getId());
			q.setParameter("lectureId", lecture.getId());

			return q.getResultList();
		}
	}

}
