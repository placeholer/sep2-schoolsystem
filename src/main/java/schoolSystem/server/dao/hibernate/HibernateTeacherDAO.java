package schoolSystem.server.dao.hibernate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import schoolSystem.server.dao.TeacherDAO;
import schoolSystem.shared.domain.AccountState;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.StringComparator;

public class HibernateTeacherDAO extends AbstractHibernateDAO<Teacher> implements TeacherDAO {

	public HibernateTeacherDAO(SessionFactory sf) {
		super(sf, Teacher.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Teacher> getByName(String name) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT t FROM Teacher t WHERE t.name LIKE :name");
			q.setParameter("name", name);

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Teacher> getActive() {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT t FROM Teacher t WHERE t.accountState LIKE 'ACTIVE'");

			return q.getResultList();
		}
	}

	@Override
	public void setAccountState(Long id, AccountState state) {
		Transaction tx = null;

		try(Session s = getSession()) {
			tx = s.beginTransaction();

			Query q = s.createQuery("UPDATE Teacher t SET t.accountState = :state");
			q.setParameter("state", state);

			q.executeUpdate();

			tx.commit();
		} catch(HibernateException e) {
			if(null != tx)
				tx.rollback();
		}
	}

	@Override
	public List<SearchItem<Teacher>> search(String key) {
		try(Session s = getSession()) {
			Stream<Teacher> teachers = s.createQuery("SELECT t FROM Teacher t", Teacher.class).stream();
			StringComparator sc = new StringComparator();
			return teachers
				.map(st -> new SearchItem<Teacher>(st, sc.compare(key, st.getName())))
				.filter(i -> i.score >= 0)
				.sorted()
				.collect(Collectors.toList());
		}
	}
}
