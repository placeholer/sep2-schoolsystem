package schoolSystem.server.dao.hibernate;

import java.util.function.BooleanSupplier;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.logging.Logger;

import schoolSystem.server.dao.AbstractDAOFactory;
import schoolSystem.server.dao.AttendanceDAO;
import schoolSystem.server.dao.CourseDAO;
import schoolSystem.server.dao.GradeDAO;
import schoolSystem.server.dao.LectureDAO;
import schoolSystem.server.dao.PasswordDAO;
import schoolSystem.server.dao.StudentDAO;
import schoolSystem.server.dao.TeacherDAO;
import schoolSystem.shared.util.Procedure;

/**
 * {@link schoolSystem.server.dao.AbstractDAOFactory AbstractDAOFactory} implementation for Hibernate
 */
public class HibernateDAOFactory implements AbstractDAOFactory {

	private SessionFactory sfactory;

	private StudentDAO studentdao;
	private TeacherDAO teacherdao;
	private GradeDAO gradedao;
	private AttendanceDAO attendancedao;
	private CourseDAO coursedao;
	private LectureDAO lecturedao;
	private PasswordDAO passworddao;

	private static final Logger LOG = Logger.getLogger(HibernateDAOFactory.class.getName());

	/**
	 * Default constructor. Initializes SessionFactory with configuration from hibernate.cfg.xml file
	 */
	public HibernateDAOFactory() {
		LOG.info("Creating new Hibernate DAO factory");

		Configuration cfg = new Configuration().configure(getClass().getResource("/hibernate.cfg.xml"));
		sfactory = cfg.buildSessionFactory();
	}

	@Override
	public StudentDAO getStudentDAO() {
		synchronizedIf(() -> null == studentdao, () -> {
			LOG.info("Initializing HibernateStudentDAO");
			studentdao = new HibernateStudentDAO(sfactory);
		});
		return studentdao;
	}

	@Override
	public TeacherDAO getTeacherDAO() {
		synchronizedIf(() -> null == teacherdao, () -> {
			LOG.info("Initializing HibernateTeacherDAO");
			teacherdao = new HibernateTeacherDAO(sfactory);
		});
		return teacherdao;
	}

	@Override
	public GradeDAO getGradeDAO() {
		synchronizedIf(() -> null == gradedao, () -> {
			LOG.info("Initializing HibernateGradeDAO");
			gradedao = new HibernateGradeDAO(sfactory);
		});
		return gradedao;
	}

	@Override
	public AttendanceDAO getAttendanceDAO() {
		synchronizedIf(() -> null == attendancedao, () -> {
			LOG.info("Initializing HibernateAttendanceDAO");
			this.attendancedao = new HibernateAttendanceDAO(sfactory);
		});
		return attendancedao;
	}

	@Override
	public CourseDAO getCourseDAO() {
		synchronizedIf(() -> null == coursedao, () -> {
			LOG.info("Initializing HibernateCourseDAO");
			this.coursedao = new HibernateCourseDAO(sfactory);
		});
		return coursedao;
	}

	@Override
	public LectureDAO getLectureDAO() {
		synchronizedIf(() -> null == lecturedao, () -> {
			LOG.info("Initializing HibernateLectureDAO");
			this.lecturedao = new HibernateLectureDAO(sfactory);
		});
		return lecturedao;
	}

	@Override
	public PasswordDAO getPasswordDAO() {
		synchronizedIf(() -> null == passworddao, () -> {
			LOG.info("Initializing HibernatePasswordDAO");
			this.passworddao = new HibernatePasswordDAO(sfactory);
		});
		return passworddao;
	}

	/**
	 * Execute Procedure in synchronized block if condition is true
	 *
	 * @param condition	if evaluates to true acquire the lock
	 * @param p	Procedure to execure in a critical section
	 */
	private void synchronizedIf(BooleanSupplier condition, Procedure p) {
		if(condition.getAsBoolean()) {
			synchronized(this) {
				if(condition.getAsBoolean())
					p.exec();
			}
		}
	}

	/**
	 * Closes SessionFactory
	 */
	public void close() {
		LOG.info("Closing the SessionFactory");
		sfactory.close();
	}
}
