package schoolSystem.server.dao.hibernate;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import schoolSystem.server.dao.GradeDAO;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Grade;
import schoolSystem.shared.domain.Student;

public class HibernateGradeDAO extends AbstractHibernateDAO<Grade> implements GradeDAO {

	public HibernateGradeDAO(SessionFactory sf) {
		super(sf, Grade.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Grade> getByStudent(Student st) {
		try(Session s = getSession()) {
			CriteriaBuilder cb = s.getCriteriaBuilder();
			CriteriaQuery<Grade> cq = cb.createQuery(Grade.class);
			Root<Grade> root = cq.from(Grade.class);
			cq.select(root).where(cb.equal(root.get("student"), st));
			Query q = s.createQuery(cq);

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Grade> getByCourse(Course course) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT g FROM Grade g WHERE g.course.id = " + course.getId());

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Grade> getForStudentByCourse(Student st, Course course) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT g FROM Grade g WHERE g.student.id = :studentId AND g.course.id = :courseId");
			q.setParameter("studentId", st.getId());
			q.setParameter("courseId", course.getId());

			return q.getResultList();
		}
	}
}
