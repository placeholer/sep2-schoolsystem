package schoolSystem.server.dao.hibernate;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import schoolSystem.server.dao.DAO;

/**
 * Abstract base class for Hibernate-based DAO's
 */
public abstract class AbstractHibernateDAO<T> implements DAO<T> {

	/**
	 * SessionFactory object to acquire Sessions from
	 */
	private SessionFactory sfactory;

	/**
	 * Target class
	 */
	private Class<T> cls;

	/**
	 * Initialize AbstractHibernateDAO with SessionFactory and Class
	 *
	 * @param sfactory	SessionFactory to use
	 * @param cls	target class
	 */
	public AbstractHibernateDAO(SessionFactory sfactory, Class<T> cls) {
		this.sfactory = sfactory;
		this.cls = cls;
	}

	@Override
	public T get(Long id) {
		try(Session s = getSession()) {
			return s.find(cls, id);
		}
	}

	@Override
	public List<T> getAll() {
		try(Session s = getSession()) {

			CriteriaBuilder cb = s.getCriteriaBuilder();
			CriteriaQuery<T> cq = cb.createQuery(cls);
			cq.select(cq.from(cls));
			Query<T> q = s.createQuery(cq);

			return q.getResultList();
		}
	}

	@Override
	public void persist(T obj) {
		Session s = getSession();
		Transaction tx = null;

		try(s) {
			tx = s.beginTransaction();

			s.persist(obj);

			tx.commit();
		} catch(HibernateException e) {
			if(null != tx)
				tx.rollback();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void delete(T obj) {
		Session s = getSession();
		Transaction tx = null;

		try(s) {
			tx = s.beginTransaction();

			s.delete(obj);

			tx.commit();
		} catch(HibernateException e) {
			if(null != tx)
				tx.rollback();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void update(T obj) {
		Session s = getSession();
		Transaction tx = null;

		try(s) {
			tx = s.beginTransaction();

			s.update(obj);

			tx.commit();
		} catch(HibernateException e) {
			if(null != tx)
				tx.rollback();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Gets Session object
	 *
	 * @return hibernate Session
	 */
	protected Session getSession() {
		return sfactory.openSession();
	}

	/**
	 * Closes SessionFactory
	 */
	public void close() {
		sfactory.close();
	}
}
