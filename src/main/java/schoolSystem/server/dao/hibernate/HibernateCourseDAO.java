package schoolSystem.server.dao.hibernate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import schoolSystem.server.dao.CourseDAO;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.StringComparator;

public class HibernateCourseDAO extends AbstractHibernateDAO<Course> implements CourseDAO {

	public HibernateCourseDAO(SessionFactory sf) {
		super(sf, Course.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Course> getForTeacher(Teacher teacher) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT c FROM Course c WHERE :teacher MEMBER OF c.teachers");
			q.setParameter("teacher", teacher);

			return q.getResultList();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Course> getForStudent(Student st) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT c FROM Course c WHERE :student MEMBER OF c.students");
			q.setParameter("student", st);

			return q.getResultList();
		}
	}

	@Override
	public List<SearchItem<Course>> search(String key) {
		try(Session s = getSession()) {
			Stream<Course> courses = s.createQuery("SELECT c FROM Course c", Course.class).stream();
			StringComparator sc = new StringComparator();
			List<SearchItem<Course>> res = courses
				.map(st -> new SearchItem<Course>(st, sc.compare(key, st.getName())))
				.filter(i -> i.score >= 0)
				.sorted()
				.collect(Collectors.toList());



			return res;
		}
	}
}
