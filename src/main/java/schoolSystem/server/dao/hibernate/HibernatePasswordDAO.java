package schoolSystem.server.dao.hibernate;

import javax.persistence.Query;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import schoolSystem.server.dao.PasswordDAO;
import schoolSystem.shared.domain.Password;
import schoolSystem.shared.domain.User;

public class HibernatePasswordDAO extends AbstractHibernateDAO<Password> implements PasswordDAO {

	public HibernatePasswordDAO(SessionFactory sf) {
		super(sf, Password.class);
	}

	@Override
	public Password getForUser(User u) {
		try(Session s = getSession()) {
			Query q = s.createQuery("SELECT p FROM Password p WHERE p.user.id = :id");
			q.setParameter("id", u.getId());

			return (Password) q.getSingleResult();
		}
	}

	@Override
	public void deleteForUser(User u) {
		Transaction tx = null;
		try(Session s = getSession()) {
			tx = s.beginTransaction();

			Query q = s.createQuery("DELETE FROM Password p WHERE p.user = :user");
			q.setParameter("user", u);
			q.executeUpdate();

			tx.commit();
		} catch(HibernateException e) {
			if(null != tx)
				tx.rollback();
		}
	}

	@Override
	public void updateForUser(User u, Password p) {
		deleteForUser(u);
		persist(p);
	}
}
