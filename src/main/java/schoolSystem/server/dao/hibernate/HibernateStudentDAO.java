package schoolSystem.server.dao.hibernate;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import schoolSystem.server.dao.StudentDAO;
import schoolSystem.shared.domain.AccountState;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.StringComparator;

public class HibernateStudentDAO extends AbstractHibernateDAO<Student> implements StudentDAO {

	public HibernateStudentDAO(SessionFactory sf) {
		super(sf, Student.class);
	}

	@Override
	public List<Student> getByName(String name) {
		try(Session s = getSession()) {
			TypedQuery<Student> q = s.createQuery("SELECT s FROM Student s WHERE name LIKE :name", Student.class);
			q.setParameter("name", name);

			return q.getResultList();
		}
	}

	@Override
	public List<Student> getActive() {
		try(Session s = getSession()) {
			TypedQuery<Student> q = s.createQuery("SELECT s FROM Student s WHERE s.accountState LIKE 'ACTIVE'", Student.class);

			return q.getResultList();
		}
	}

	@Override
	public void setAccountState(Long id, AccountState state) {
		Transaction tx = null;

		try(Session s = getSession()) {
			tx = s.beginTransaction();

			Query q = s.createQuery("UPDATE Student s SET s.accountState = :state");
			q.setParameter("state",  state);

			q.executeUpdate();

			tx.commit();
		} catch(HibernateException e) {
			if(null != tx)
				tx.rollback();
		}
	}

	@Override
	public List<SearchItem<Student>> search(String key) {
		try(Session s = getSession()) {
			Stream<Student> students = s.createQuery("SELECT s FROM Student s", Student.class).stream();
			StringComparator sc = new StringComparator();
			return students
				.map(st -> new SearchItem<Student>(st, sc.compare(key, st.getName())))
				.filter(i -> i.score >= 0)
				.sorted()
				.collect(Collectors.toList());
		}
	}
}
