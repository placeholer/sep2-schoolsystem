package schoolSystem.server.dao;

import java.util.List;

import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.search.SearchProvider;

/**
 * Base interface for Course DAO's
 */
public interface CourseDAO extends DAO<Course>, SearchProvider<Course, String> {

	/**
	 * Gets Courses a Teacher is qualified to teach
	 *
	 * @param teacher	the Teacher
	 * @return list of Courses
	 */
	public List<Course> getForTeacher(Teacher teacher);

	/**
	 * Gets Courses the Student attends
	 *
	 * @param student	the Student
	 * @return list of Courses
	 */
	public List<Course> getForStudent(Student student);
}
