package schoolSystem.server.dao;

import schoolSystem.server.dao.hibernate.HibernateDAOFactory;

/**
 * Factory class for AbstractDAOFactory implementations
 */
public class DAOFactoryProducer {

	/**
	 * Gets new Hibernate-based DAO factory
	 */
	public static HibernateDAOFactory newHibernateDAOFactory() {
		return new HibernateDAOFactory();
	}
}
