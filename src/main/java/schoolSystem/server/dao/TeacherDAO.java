package schoolSystem.server.dao;

import java.util.List;

import schoolSystem.shared.domain.AccountState;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.search.SearchProvider;

/**
 * Base interface for Teacher DAO's
 */
public interface TeacherDAO extends DAO<Teacher>, SearchProvider<Teacher, String> {

	/**
	 * Gets list of Teachers by name
	 *
	 * @param name	name of the Teacher
	 * @return list of Teachers
	 */
	public List<Teacher> getByName(String name);

	/**
	 * Gets list of Teachers with ACTIVE account
	 *
	 * @return list of Teachers
	 */
	public List<Teacher> getActive();

	/**
	 * Updates the state of Teacher's account
	 *
	 * @param id	ID of the teacher
	 * @param state	new AccountState
	 */
	public void setAccountState(Long id, AccountState state);
}
