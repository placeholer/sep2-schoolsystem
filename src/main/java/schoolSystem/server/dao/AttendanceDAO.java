package schoolSystem.server.dao;

import java.time.LocalDate;
import java.util.List;

import schoolSystem.shared.domain.Attendance;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;

/**
 * Base interface for Attendance DAO's
 */
public interface AttendanceDAO extends DAO<Attendance> {

	/**
	 * Gets attendance of a Student
	 *
	 * @param student	the Student
	 * @return list of Attendance
	 */
	public List<Attendance> getByStudent(Student student);

	/**
	 * Gets Attendance for a Course
	 *
	 * @param course	the Course
	 * @return list of Attendance
	 */
	public List<Attendance> getByCourse(Course course);

	/**
	 * Gets Attendance for a Lecture
	 *
	 * @param lecture	the Lecture
	 * @return list of Attendance
	 */
	public List<Attendance> getByLecture(Lecture lecture);

	/**
	 * Gets attendance of a Student at given date
	 *
	 * @param student	the Student
	 * @param date	the date of attendance
	 * @return list of Attendance
	 */
	public List<Attendance> getForStudentByDate(Student student, LocalDate date);

	/**
	 * Gets attendance of a Student for a Course
	 *
	 * @param student	the Student
	 * @param course	the Course
	 * @return list of Attendance
	 */
	public List<Attendance> getForStudentByCourse(Student student, Course course);

	/**
	 * Gets attendance for a Student for a Lecture
	 *
	 * @param student	the Student
	 * @param lecture	the Lecture
	 * @return list of Attendance
	 */
	public List<Attendance> getForStudentByLecture(Student student, Lecture lecture);
}
