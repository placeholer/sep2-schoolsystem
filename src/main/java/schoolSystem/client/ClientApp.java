package schoolSystem.client;

import javafx.application.Application;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import schoolSystem.client.view.GUIViewFactory;
import schoolSystem.client.view.GUIViewManager;
import schoolSystem.client.viewmodel.ViewModelFactory;
import schoolSystem.client.viewmodel.ViewModelFactoryImpl;

public class ClientApp extends Application {
	private ViewModelFactory vmf;
	private GUIViewFactory vf;
	private GUIViewManager vm;
	private Client client;

	private static ClientApp app;

	public ClientApp() {
		app = this;
	}

	public static ClientApp getApp() {
		return app;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Client getClient() {
		return client;
	}

	@Override
	public void start(Stage primaryStage) {
		vmf = new ViewModelFactoryImpl();
		vf = new GUIViewFactory(vmf);
		vm = new GUIViewManager(primaryStage, vf);
		vm.openLoginView();
	}

	@Override
	public void stop() {
		try {
			if(client.isConnected())
				client.disconnect();
		} catch (ConnectionException e) {
			vm.openDialog(AlertType.ERROR, "Failed to disconnect from server", e.toString());
		} finally {
			// Force JavaFX Application Thread to shutdown
			System.exit(0);
		}
	}
}
