package schoolSystem.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import schoolSystem.client.viewmodel.LectureListViewModel;
import schoolSystem.shared.domain.Lecture;

public class LectureListView extends FxmlView {

	@FXML private ListView<Lecture> lectureList;
	@FXML private TextArea description;

	private LectureListViewModel viewModel;
	private ViewManager viewManager;

	public LectureListView(LectureListViewModel viewModel, ViewManager viewManager) {
		super("/fxml/lectureList.fxml", "Lectures");
		this.viewModel = viewModel;
		this.viewManager = viewManager;
	}

	@FXML
	private void initialize() {
		description.textProperty().bindBidirectional(viewModel.descriptionProperty());

		lectureList.setCellFactory(lv -> new ListCell<Lecture>() {
			@Override
			public void updateItem(Lecture l, boolean empty) {
				super.updateItem(l, empty);
				if(empty)
					setText(null);
				else
					setText(l.getSubject());
			}
		});
		lectureList.setItems(viewModel.lectureList());
		viewModel.selectedProperty().bind(lectureList.getSelectionModel().selectedItemProperty());
	}

	@FXML
	public void OnAddButton() {
		viewManager.openAddLectureView(viewModel.getCourse(), null);
	}

	@FXML
	public void OnRemoveButton() {
		viewModel.remove();
	}

	@FXML
	public void OnEditButton() {
		viewManager.openAddLectureView(viewModel.getCourse(), viewModel.selectedProperty().get());
	}

	@FXML
	public void OnCloseButton() {
		viewManager.close(this);
	}
}
