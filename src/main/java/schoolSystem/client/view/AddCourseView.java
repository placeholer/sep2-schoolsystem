package schoolSystem.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import schoolSystem.client.viewmodel.AddCourseViewModel;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.search.SearchItem;

public class AddCourseView extends FxmlView {
	@FXML private TextField name;
	@FXML private TextArea description;
	@FXML private ListView<Student> students;
	@FXML private ListView<Teacher> teachers;

	private AddCourseViewModel viewmodel;
	private ViewManager viewManager;

	public AddCourseView(AddCourseViewModel viewmodel, ViewManager viewManager) {
		super("/fxml/addCourse.fxml", "Edit course");
		this.viewmodel = viewmodel;
		this.viewManager = viewManager;
	}

	@FXML
	private void initialize() {
		name.textProperty().bindBidirectional(viewmodel.courseNameProperty());
		description.textProperty().bindBidirectional(viewmodel.courseDescriptionProperty());

		students.setCellFactory(lv -> new ListCell<Student>() {
			@Override
			public void updateItem(Student st, boolean empty) {
				super.updateItem(st, empty);
				if(empty)
					setText(null);
				else
					setText(st.getName());
			}
		});
		students.setItems(viewmodel.observableStudentList());

		teachers.setCellFactory(lv -> new ListCell<Teacher>() {
			@Override
			public void updateItem(Teacher st, boolean empty) {
				super.updateItem(st, empty);
				if(empty)
					setText(null);
				else
					setText(st.getName());
			}
		});
		teachers.setItems(viewmodel.observableTeacherList());
	}

	@FXML
	public void addTeacher() {
		viewManager.openSearchView(viewmodel.teacherSearchProvider(), item -> {
			if(null != item)
				viewmodel.observableTeacherList().add(item.item);
		}, lv -> new ListCell<SearchItem<Teacher>>() {
			@Override
			public void updateItem(SearchItem<Teacher> t, boolean empty) {
				super.updateItem(t, empty);
				if(empty)
					setText(null);
				else
					setText(t.item.getName());
			}
		});
	}

	@FXML
	public void addStudent() {
		viewManager.openSearchView(viewmodel.studentSearchProvider(), item -> {
			if(null != item)
				viewmodel.observableStudentList().add(item.item);
		}, lv -> new ListCell<SearchItem<Student>>() {
			@Override
			public void updateItem(SearchItem<Student> t, boolean empty) {
				super.updateItem(t, empty);
				if(empty)
					setText(null);
				else
					setText(t.item.getName());
			}
		});
	}

	@FXML
	private void removeTeacher() {
		teachers.getItems().removeAll(teachers.getSelectionModel().getSelectedItems());
	}

	@FXML
	private void removeStudent() {
		students.getItems().removeAll(students.getSelectionModel().getSelectedItems());
	}

	@FXML
	public void save() {
		viewmodel.save();
		viewManager.close(this);
	}

	@FXML
	public void close() {
		viewManager.close(this);
	}

	@Override
	public String toString() {
		return "ADD COURSE VIEW";
	}
}
