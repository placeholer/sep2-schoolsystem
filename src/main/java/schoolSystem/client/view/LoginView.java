package schoolSystem.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import schoolSystem.client.ConnectionException;
import schoolSystem.client.viewmodel.LoginViewModel;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.security.InvalidPasswordException;
import schoolSystem.shared.security.InvalidUsernameException;

public class LoginView extends FxmlView {

	@FXML private TextField server;
	@FXML private TextField port;
	@FXML private TextField username;
	@FXML private TextField password;

	private LoginViewModel viewModel;
	private GUIViewManager viewManager;

	public LoginView(LoginViewModel viewModel, GUIViewManager viewManager)
	{
		super("/fxml/loginView.fxml", "Login");
		this.viewModel = viewModel;
		this.viewManager = viewManager;
	}

	@FXML
	private void initialize() {
		server.textProperty().bindBidirectional(viewModel.getLoginViewServer());
		port.textProperty().bindBidirectional(viewModel.portProperty(), new StringConverter<Number>() {

			@Override
			public String toString(Number i) {
				return i.toString();
			}

			@Override
			public Integer fromString(String s) {
				return Integer.parseInt(s);
			}

		});
		username.textProperty().bindBidirectional(viewModel.getLoginViewUsername());
		password.textProperty().bindBidirectional(viewModel.getLoginViewPassword());
	}

	@FXML
	public void onCloseButton() {
		viewManager.close(this);
	}

	@FXML
	public void onLoginButton() {
		try {
			User user = viewModel.login();

			viewManager.openCalendarView(user);
		} catch(InvalidUsernameException e) {
			viewManager.openDialog(AlertType.ERROR,  "Could not log in", "Such user does not exist");
		} catch(InvalidPasswordException e) {
			viewManager.openDialog(AlertType.ERROR,  "Could not log in", "Password is invalid");
		} catch (ConnectionException e) {
			viewManager.openDialog(AlertType.ERROR, "Could not connect to serer", e.toString());
		}
	}

	@Override
	public String toString() {
		return "LOGIN";
	}
}
