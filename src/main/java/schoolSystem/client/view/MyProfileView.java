package schoolSystem.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import schoolSystem.client.viewmodel.MyProfileViewModel;

public class MyProfileView extends FxmlView {

	@FXML private Label name;
	@FXML private Label username;

	private MyProfileViewModel viewModel;
	private ViewManager viewManager;

	public MyProfileView(MyProfileViewModel viewModel, ViewManager viewManager) {
		super("/fxml/myProfile.fxml", "My profile");
		this.viewModel = viewModel;
		this.viewManager = viewManager;
	}

	@FXML
	private void initialize() {
		username.textProperty().bindBidirectional(viewModel.usernameProperty());
	}

	@FXML
	private void closeButton() {
		viewManager.close(this);
	}
}
