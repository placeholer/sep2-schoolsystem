package schoolSystem.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import schoolSystem.client.viewmodel.CreateUserViewModel;

public class CreateUserView extends FxmlView {

	@FXML private TextField username;
	@FXML private CheckBox isTeacher;

	private CreateUserViewModel viewmodel;
	private ViewManager viewmanager;

	public CreateUserView(CreateUserViewModel viewmodel, ViewManager viewmanager) {
		super("/fxml/createUser.fxml", "Create user");
		this.viewmodel = viewmodel;
		this.viewmanager = viewmanager;
	}

	@FXML
	private void initialize() {
		username.textProperty().bindBidirectional(viewmodel.usernameProperty());
		isTeacher.selectedProperty().bindBidirectional(viewmodel.isTeacherProperty());
	}

	@FXML
	private void create() {
		viewmodel.create();
	}

	@FXML
	private void close() {
		viewmanager.close(this);
	}
}
