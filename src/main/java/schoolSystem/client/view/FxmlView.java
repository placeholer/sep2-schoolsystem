package schoolSystem.client.view;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

public abstract class FxmlView implements View {

	private String path;
	private String title;

	private Parent root;

	public FxmlView(String path, String title) {
		this.path = path;
		this.title = title;
	}

	public Parent load() throws IOException {
		if(null == root) {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
			loader.setController(this);
			return loader.load();
		}
		return root;
	}

	public String getPath() {
		return path;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public String getId() {
		return path;
	}
}
