package schoolSystem.client.view;

import java.time.LocalDate;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import schoolSystem.client.viewmodel.CourseListViewModel;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;

public class CourseListView extends FxmlView {

	@FXML private ListView<Course> courses;
	@FXML private TextArea courseDescription;
	@FXML private Button edit;
	@FXML private Button remove;
	@FXML private Button seeLectures;

	@FXML private TableView<Lecture> lectures;
	@FXML private TableColumn<Lecture, LocalDate> lectureDate;
	@FXML private TableColumn<Lecture, String> lectureSubject;
	@FXML private TableColumn<Lecture, String> lectureTeacher;


	private CourseListViewModel viewModel;
	private GUIViewManager viewManager;

	public CourseListView(CourseListViewModel viewModel, GUIViewManager viewManager) {
		super("/fxml/courseList.fxml", "Courses");
		this.viewModel = viewModel;
		this.viewManager = viewManager;
	}

	@FXML
	private void initialize() {
		courseDescription.textProperty().bindBidirectional(viewModel.courseDescriptionProperty());

		courses.setCellFactory(lv -> new ListCell<Course>() {
			@Override
			public void updateItem(Course item, boolean empty) {
				super.updateItem(item, empty);
				if(empty) {
					setText(null);
					setTooltip(null);
				} else {
					setText(item.getName());
					setTooltip(new Tooltip(item.getName()));
				}
			}
		});
		courses.setItems(viewModel.getObservableCourseList());

		lectures.setItems(viewModel.getLectures());
		lectureDate.setCellValueFactory(new PropertyValueFactory<Lecture, LocalDate>("date"));
		lectureSubject.setCellValueFactory(new PropertyValueFactory<Lecture, String>("subject"));
		lectureTeacher.setCellValueFactory(cell -> new ReadOnlyStringWrapper(cell.getValue().getTeacher().getName()));

		viewModel.selectedCourseProperty().bind(courses.getSelectionModel().selectedItemProperty());
		edit.disableProperty().bind(courses.getSelectionModel().selectedItemProperty().isNull());
		remove.disableProperty().bind(courses.getSelectionModel().selectedItemProperty().isNull());
		seeLectures.disableProperty().bind(courses.getSelectionModel().selectedItemProperty().isNull());
	}

	@FXML
	private void onAddButton() {
		viewManager.openAddCourseView(null);
	}

	@FXML
	private void onRemoveButton() {
		viewModel.remove();
	}

	@FXML
	private void onEditButton() {
		viewManager.openAddCourseView(courses.getSelectionModel().getSelectedItem());
	}

	@FXML
	private void onCloseButton() {
		viewManager.close(this);
	}

	@FXML
	private void seeLectures() {
		viewManager.openLectureListView(viewModel.selectedCourseProperty().get());
	}
}
