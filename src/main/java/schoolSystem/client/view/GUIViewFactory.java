package schoolSystem.client.view;

import java.util.function.Consumer;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import schoolSystem.client.viewmodel.ViewModelFactory;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.SearchProvider;

public class GUIViewFactory implements ViewFactory {

	private ViewModelFactory vmf;
	private GUIViewManager viewManager;

	public GUIViewFactory(ViewModelFactory vmf) {
		this.vmf = vmf;
	}

	public void setViewManager(GUIViewManager vm) {
		this.viewManager = vm;
	}

	@Override
	public AddCourseView getAddCourseView(Course c) {
		return new AddCourseView(vmf.getAddCourseViewModel(c), viewManager);
	}

	@Override
	public LectureListView getLectureListView(Course c) {
		return new LectureListView(vmf.getLectureListViewModel(c), viewManager);
	}

	@Override
	public CalendarView getCalendarView(User user) {
		return new CalendarView(vmf.getCalendarViewModel(user), viewManager);
	}

	@Override
	public LoginView getLoginView() {
		return new LoginView(vmf.getLoginViewModel(), viewManager);
	}

	@Override
	public CourseListView getCourseListView(User u) {
		return new CourseListView(vmf.getCourseListViewModel(u), viewManager);
	}

	@Override
	public AddLectureView getAddLectureView(Course c, Lecture l) {
		return new AddLectureView(vmf.getAddLectureViewModel(c, l), viewManager);
	}

	@Override
	public UsersListView getUsersListView() {
		return new UsersListView(vmf.getUsersListViewModel(), viewManager);
	}

	@Override
	public <T> SearchView<T> getSearchView(SearchProvider<T, String> provider, Consumer<SearchItem<T>> callback,
			Callback<ListView<SearchItem<T>>, ListCell<SearchItem<T>>> cellFactory) {
		return new SearchView<T>(vmf.getSearchViewModel(provider, callback), viewManager, cellFactory);
	}

	@Override
	public CreateUserView getCreateUserView(User user) {
		return new CreateUserView(vmf.getCreateUserViewModel(user), viewManager);
	}

	@Override
	public MyProfileView getMyProfileView(User u) {
		return new MyProfileView(vmf.getMyProfileViewModel(u), viewManager);
	}
}
