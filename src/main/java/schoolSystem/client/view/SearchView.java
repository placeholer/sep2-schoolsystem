package schoolSystem.client.view;

import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import schoolSystem.client.viewmodel.SearchViewModel;
import schoolSystem.shared.search.SearchItem;

public class SearchView<T> extends FxmlView {

	@FXML private TextField phrase;
	@FXML private ListView<SearchItem<T>> results;

	private SearchViewModel<T> viewmodel;
	private GUIViewManager vm;
	private Callback<ListView<SearchItem<T>>, ListCell<SearchItem<T>>> cellFactory;

	public SearchView(SearchViewModel<T> viewmodel, GUIViewManager vm, Callback<ListView<SearchItem<T>>, ListCell<SearchItem<T>>> cellFactory) {
		super("/fxml/search.fxml", "Search...");
		this.viewmodel = viewmodel;
		this.vm = vm;
		this.cellFactory = cellFactory;
	}

	@FXML
	private void initialize() {
		viewmodel.phraseProperty().bindBidirectional(phrase.textProperty());

		phrase.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
			switch(e.getCode()) {
				case DOWN:
					e.consume();
					selectNext();
					break;
				case UP:
					e.consume();
					selectPrev();
					break;
				case ENTER:
					confirm();
					break;
				case ESCAPE:
					vm.close(SearchView.this);
					break;
			}
		});

		viewmodel.selectedProperty().bind(results.getSelectionModel().selectedItemProperty());

		results.setCellFactory(cellFactory);

		results.setOnMouseClicked(e -> confirm());

		results.setItems(viewmodel.resultsProperty());
	}

	private void selectNext() {
		SelectionModel<SearchItem<T>> model = results.getSelectionModel();
		if(model.getSelectedIndex() == -1 || model.getSelectedIndex() == results.getItems().size() - 1) {
			model.selectFirst();
		} else {
			model.selectNext();
		}
	}

	private void selectPrev() {
		SelectionModel<SearchItem<T>> model = results.getSelectionModel();
		if(model.getSelectedIndex() == -1 || model.getSelectedIndex() == 0) {
			model.selectLast();
		} else {
			model.selectPrevious();
		}
	}

	public SearchViewModel<T> getViewModel() {
		return viewmodel;
	}

	@FXML
	private void confirm() {
		viewmodel.confirm();
		vm.close(this);
	}

	@FXML
	private void close() {
		vm.close(this);
	}
}
