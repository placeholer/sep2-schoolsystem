package schoolSystem.client.view;

import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyLongWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import schoolSystem.client.viewmodel.UsersListViewModel;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;

public class UsersListView extends FxmlView {

	@FXML private TableView<User> users;
	@FXML private TableColumn<User, Number> userId;
	@FXML private TableColumn<User, String> userName;
	@FXML private TableColumn<User, Boolean> isTeacher;

	private UsersListViewModel viewmodel;
	private ViewManager viewManager;

	public UsersListView(UsersListViewModel viewmodel, ViewManager viewManager) {
		super("/fxml/usersList.fxml", "Users");
		this.viewmodel = viewmodel;
		this.viewManager = viewManager;
	}

	@FXML
	private void initialize() {
		users.setItems(viewmodel.usersList());

		userId.setCellValueFactory(cell -> new ReadOnlyLongWrapper(cell.getValue().getId()));
		userName.setCellValueFactory(cell -> new ReadOnlyStringWrapper(cell.getValue().getName()));
		isTeacher.setCellValueFactory(cell -> new ReadOnlyBooleanWrapper(cell.getValue() instanceof Teacher));

		viewmodel.selectedUserProperty().bind(users.getSelectionModel().selectedItemProperty());
	}

	@FXML
	private void addUser() {
		viewManager.openCreateUserView(null);
	}

	@FXML
	private void edit() {
		viewManager.openCreateUserView(viewmodel.selectedUserProperty().get());
	}

	@FXML
	private void remove() {
		viewmodel.remove();
	}

	@FXML
	private void closeButton() {
		viewManager.close(this);
	}
}
