package schoolSystem.client.view;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.SearchProvider;

public class GUIViewManager implements ViewManager {

	private ViewFactory vf;
	private Stage primaryStage;

	private String title = "SchoolSystem";

	private Map<Stage, LinkedList<FxmlView>> stages;
	private Map<View, Stage> views;

	public GUIViewManager(Stage primaryStage, GUIViewFactory vf) {
		this.primaryStage = primaryStage;
		this.vf = vf;
		vf.setViewManager(this);

		this.stages = new ConcurrentHashMap<>();
		this.views = new ConcurrentHashMap<>();
	}

	@Override
	public void openLoginView() {
		openInStage(vf.getLoginView(), primaryStage);
	}

	@Override
	public void openCourseListView(User user) {
		openInStage(vf.getCourseListView(user), primaryStage);
	}

	@Override
	public void openAddCourseView(Course course) {
		openInStage(vf.getAddCourseView(course), new Stage());
	}

	@Override
	public void openLectureListView(Course c) {
		openInStage(vf.getLectureListView(c), primaryStage);
	}

	@Override
	public void openAddLectureView(Course c, Lecture l) {
		openInStage(vf.getAddLectureView(c, l), new Stage());
	}

	@Override
	public void openCalendarView(User user) {
		openInStage(vf.getCalendarView(user), primaryStage);
	}

	@Override
	public void openCreateUserView(User u) {
		openInStage(vf.getCreateUserView(u), primaryStage);
	}

	@Override
	public void openUserListView() {
		openInStage(vf.getUsersListView(), primaryStage);
	}

	@Override
	public void openMyProfile(User user) {
		openInStage(vf.getMyProfileView(user), new Stage());
	}

	@Override
	public <T> void openSearchView(SearchProvider<T, String> sp, Consumer<SearchItem<T>> callback,
			Callback<ListView<SearchItem<T>>, ListCell<SearchItem<T>>> cellFactory) {
		Stage s = new Stage();
		s.initModality(Modality.APPLICATION_MODAL);
		openInStage(vf.getSearchView(sp, callback, cellFactory), s);
	}

	private void display(FxmlView view, Stage stage) {
		try {
			Parent root = view.load();
			stage.setScene(new Scene(root));
			stage.setTitle(title + " - " + view.getTitle());
		} catch(IOException e) {
			openDialog(AlertType.ERROR, "An error occured while opening a scene", e.toString());
			e.printStackTrace();
			Platform.exit();
		}
	}

	public void openDialog(AlertType type, String header, String content) {
		Alert alert = new Alert(type);
		alert.setResizable(true);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.showAndWait();
	}

	private Stage openInStage(FxmlView view, Stage s) {
		display(view, s);

		if(stages.get(s) == null) {
			if(s != primaryStage)
				s.initOwner(primaryStage);
			stages.put(s, new LinkedList<>());
		}

		if(!s.isShowing())
			s.show();

		stages.get(s).push(view);
		views.put(view, s);

		return s;
	}

	@Override
	public void close(View view) {
		Stage s = views.get(view);
		stages.get(s).pop();

		if(stages.get(s).size() == 0) {
			stages.remove(s);
			s.close();
			return;
		}

		FxmlView prev = stages.get(s).peek();
		display(prev, s);
	}
}
