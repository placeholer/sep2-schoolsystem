package schoolSystem.client.view;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import schoolSystem.client.viewmodel.AddLectureViewModel;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.search.SearchItem;

public class AddLectureView extends FxmlView {

	@FXML private TextField subject;
	@FXML private DatePicker date;
	@FXML private TextArea addLecture;
	@FXML private TextArea description;
	@FXML private TextField teacher;

	private AddLectureViewModel viewModel;
	private ViewManager viewManager;

	public AddLectureView(AddLectureViewModel viewModel, ViewManager viewManager) {
		super("/fxml/addLecture.fxml", "Edit lecture");
		this.viewModel = viewModel;
		this.viewManager = viewManager;
	}

	@FXML
	private void initialize() {
		this.date.valueProperty().bindBidirectional(viewModel.dateProperty());
		this.description.textProperty().bindBidirectional(viewModel.descriptionProperty());
		this.subject.textProperty().bindBidirectional(viewModel.subjectProperty());

		this.teacher.textProperty().bind(Bindings.createStringBinding(() ->
					viewModel.teacherProperty().get() == null ? "Teacher not selected" : viewModel.teacherProperty().get().getName(),
					viewModel.teacherProperty()));
	}

	@FXML
	private void onSelectTeacherButton() {
		viewManager.openSearchView(viewModel.teacherSearchProvider(), i -> {
			if(null != i)
				viewModel.teacherProperty().set(i.item);
		}, lv -> new ListCell<SearchItem<Teacher>>() {
			@Override
			public void updateItem(SearchItem<Teacher> t, boolean empty) {
				super.updateItem(t, empty);
				if(empty)
					setText(null);
				else
					setText(t.item.getName());
			}
		});
	}

	@FXML
	private void onSaveButton() {
		viewModel.save();
		viewManager.close(this);
	}

	@FXML
	private void onCloseButton() {
		viewManager.close(this);
	}
}
