package schoolSystem.client.view;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.stream.Stream;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import schoolSystem.client.viewmodel.CalendarViewModel;
import schoolSystem.shared.domain.Lecture;

public class CalendarView extends FxmlView {

	@FXML private DatePicker calendarDatePicker;
	@FXML private Label calendarWeek;
	@FXML private Label calendarDay1;
	@FXML private Label calendarDay2;
	@FXML private Label calendarDay3;
	@FXML private Label calendarDay4;
	@FXML private Label calendarDay5;
	@FXML private ListView<Lecture> firstList;
	@FXML private ListView<Lecture> secondList;
	@FXML private ListView<Lecture> thirdList;
	@FXML private ListView<Lecture> fourthList;
	@FXML private ListView<Lecture> fifthList;

	private CalendarViewModel viewModel;
	private ViewManager viewManager;

	public CalendarView(CalendarViewModel viewModel, ViewManager viewManager) {
		super("/fxml/calendar.fxml", "Calendar");
		this.viewModel = viewModel;
		this.viewManager = viewManager;
	}

	@FXML
	private void initialize() {
		firstList.setItems(viewModel.firstDayList());
		secondList.setItems(viewModel.secondDayList());
		thirdList.setItems(viewModel.thirdDayList());
		fourthList.setItems(viewModel.fourthDayList());
		fifthList.setItems(viewModel.fifthDayList());

		calendarWeek.textProperty().bind(Bindings.concat("Week ", viewModel.weekNumberProperty()));

		Stream.of(firstList, secondList, thirdList, fourthList, fifthList)
			.forEach(ls -> {
				ls.setCellFactory(lv -> new ListCell<Lecture>() {
					@Override
					public void updateItem(Lecture l, boolean empty) {
						super.updateItem(l, empty);
						if(empty) {
							setText(null);
							setTooltip(null);
						} else {
							setText(l.getCourse().getName() + " - " + l.getSubject());
							setTooltip(new Tooltip(l.getCourse().getName() + " - " + l.getSubject()));
						}
					}
				});
			});

		calendarDay1.textProperty().bind(
				Bindings.createStringBinding(() -> dayOfWeek(1).toString(), viewModel.currentDateProperty()));
		calendarDay2.textProperty().bind(
				Bindings.createStringBinding(() -> dayOfWeek(2).toString(), viewModel.currentDateProperty()));
		calendarDay3.textProperty().bind(
				Bindings.createStringBinding(() -> dayOfWeek(3).toString(), viewModel.currentDateProperty()));
		calendarDay4.textProperty().bind(
				Bindings.createStringBinding(() -> dayOfWeek(4).toString(), viewModel.currentDateProperty()));
		calendarDay5.textProperty().bind(
				Bindings.createStringBinding(() -> dayOfWeek(5).toString(), viewModel.currentDateProperty()));

		this.calendarDatePicker.valueProperty().bindBidirectional(viewModel.currentDateProperty());
	}

	@FXML
	private void onBackButton() {
		viewModel.back();
	}

	@FXML
	private void onForwardButton() {
		viewModel.forward();
	}

	@FXML
	private void calendarCoursesButton() {
		viewManager.openCourseListView(viewModel.getUser());
	}

	@FXML
	private void calendarCloseButton() {
		viewManager.close(this);
	}

	@FXML
	private void profile() {
		viewManager.openMyProfile(viewModel.getUser());
	}

	@FXML
	private void users() {
		viewManager.openUserListView();
	}

	private LocalDate dayOfWeek(int day) {
		return viewModel.getCurrentDate().with(DayOfWeek.MONDAY).plusDays(day - 1);
	}
}
