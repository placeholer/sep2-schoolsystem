package schoolSystem.client.view;

import java.util.function.Consumer;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.SearchProvider;

public interface ViewFactory {

	public CourseListView getCourseListView(User u);

	public MyProfileView getMyProfileView(User u);

	public AddCourseView getAddCourseView(Course c);

	public AddLectureView getAddLectureView(Course c, Lecture l);

	public LectureListView getLectureListView(Course course);

	public CalendarView getCalendarView(User user);

	public LoginView getLoginView();

	public CreateUserView getCreateUserView(User user);

	public UsersListView getUsersListView();

	public <T> SearchView<T> getSearchView(SearchProvider<T, String> provider, Consumer<SearchItem<T>> callback,
			Callback<ListView<SearchItem<T>>, ListCell<SearchItem<T>>> cellFactory);
}
