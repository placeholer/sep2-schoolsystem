package schoolSystem.client.view;

import java.util.function.Consumer;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.SearchProvider;

public interface ViewManager {

	public void openLoginView();

	public void openMyProfile(User user);

	public void openCourseListView(User user);

	public void openAddCourseView(Course course);

	public void openLectureListView(Course c);

	public void openAddLectureView(Course c, Lecture l);

	public void openCalendarView(User u);

	public void openCreateUserView(User u);

	public void openUserListView();

	public <T> void openSearchView(SearchProvider<T, String> sp, Consumer<SearchItem<T>> callback,
			Callback<ListView<SearchItem<T>>, ListCell<SearchItem<T>>> cellFactory);

	public void close(View view);

}
