package schoolSystem.client.viewmodel;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.SchoolSystemMediator;

public class UsersListViewModel {

	private ObservableList<User> users;
	private ObjectProperty<User> selected;

	private SchoolSystemMediator mediator;

	public UsersListViewModel(SchoolSystemMediator mediator) {
		this.mediator = mediator;

		users = FXCollections.observableArrayList(mediator.getAllUsers());
		selected = new SimpleObjectProperty<>();
	}

	public void remove() {
		if(selected.get() instanceof Teacher)
			mediator.removeTeacher(selected.get().getId());
		else
			mediator.removeStudent(selected.get().getId());
	}

	public ObservableList<User> usersList() {
		return users;
	}

	public ObjectProperty<User> selectedUserProperty() {
		return selected;
	}
}
