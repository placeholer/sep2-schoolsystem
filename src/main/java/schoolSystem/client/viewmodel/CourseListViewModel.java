package schoolSystem.client.viewmodel;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.ObservableMediator;
import schoolSystem.shared.util.Event;
import schoolSystem.shared.util.Listener;

public class CourseListViewModel {

	private ObservableMediator mediator;
	private User user;

	private ObservableList<Course> courses;
	private StringProperty courseDescription;
	private ObjectProperty<Course> selectedCourse;
	private ObservableList<Lecture> lectures;

	public CourseListViewModel(ObservableMediator mediator, User user) {
		this.mediator = mediator;
		this.user = user;
		this.courseDescription = new SimpleStringProperty();
		this.selectedCourse = new SimpleObjectProperty<>();
		this.courses = FXCollections.observableArrayList();
		this.lectures = FXCollections.observableArrayList();

		courseDescription.bind(Bindings.createStringBinding(
					() -> selectedCourse.get() == null ? "Select a course" : selectedCourse.get().getDescription(),
					selectedCourse));

		selectedCourse.addListener((obs, o, n) -> {
			if(null != n)
				lectures.setAll(mediator.getLecturesByCourse(n));
			else
				lectures.clear();
		});

		mediator.addListener(new Listener() {
			@Override
			public <T> void handle(Event<T> e) {
				Platform.runLater(() -> update());
			}
		});
		update();
	}

	public void update() {
		courses.setAll(mediator.getCoursesByUser(user));
	}

	public ObservableList<Course> getObservableCourseList() {
		return courses;
	}

	public StringProperty courseDescriptionProperty() {
		return courseDescription;
	}

	public ObjectProperty<Course> selectedCourseProperty() {
		return selectedCourse;
	}

	public ObservableList<Lecture> getLectures() {
		return lectures;
	}

	public void remove() {
		mediator.removeCourse(selectedCourse.get().getId());
	}
}
