package schoolSystem.client.viewmodel;

import java.util.Arrays;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.SchoolSystemMediator;

public class CreateUserViewModel {

	private SchoolSystemMediator mediator;

	private StringProperty username;
	private BooleanProperty isTeacher;
	private User user;

	public CreateUserViewModel(SchoolSystemMediator mediator) {
		this.mediator = mediator;

		username = new SimpleStringProperty();
		isTeacher = new SimpleBooleanProperty();
	}

	public CreateUserViewModel(SchoolSystemMediator mediator, User user) {
		this(mediator);

		if(null != user)
			setUser(user);
	}

	public void setUser(User user) {
		username.set(user.getName());
		isTeacher.set(user instanceof Teacher);
	}

	public StringProperty usernameProperty() {
		return username;
	}

	public BooleanProperty isTeacherProperty() {
		return isTeacher;
	}

	public void create() {
		if(null == user) {
			if(isTeacher.get()){
				mediator.addTeacher(username.get(), generatePassword(username.get()));
			} else {
				mediator.addStudent(username.get(), generatePassword(username.get()));
			}
		} else {
			user.setName(username.get());
			if(isTeacher.get())
				mediator.updateTeacher((Teacher) user);
			else
				mediator.updateStudent((Student) user);
		}
	}

	private String generatePassword(String username) {
		return Arrays.stream(username.split("\\w+"))
			.collect(String::new, String::concat, String::concat);
	}
}
