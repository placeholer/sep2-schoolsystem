package schoolSystem.client.viewmodel;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.mediator.ObservableMediator;
import schoolSystem.shared.util.Event;
import schoolSystem.shared.util.Listener;

public class LectureListViewModel {

	private ObservableMediator mediator;
	private Course course;

	private StringProperty description;
	private ObjectProperty<Lecture> selected;
	private ObservableList<Lecture> lectures;

	public LectureListViewModel(ObservableMediator mediator, Course course) {
		this.mediator = mediator;
		this.course = course;

		this.description = new SimpleStringProperty();
		this.selected = new SimpleObjectProperty<>();
		this.lectures = FXCollections.observableArrayList();

		description.bind(Bindings.createStringBinding(
					() -> selected.get() == null ? "Select a course" : selected.get().getDescription(),
					selected));

		mediator.addListener(new Listener() {
			@Override
			public <T> void handle(Event<T> e) {
				Platform.runLater(() -> update());
			}
		});
		update();
	}

	private void update() {
		lectures.setAll(mediator.getLecturesByCourse(course));
	}

	public void remove() {	
		mediator.removeLecture(selected.get().getId());
	}

	public Course getCourse() {
		return course;
	}

	public StringProperty descriptionProperty() {
		return description;
	}

	public ObservableList<Lecture> lectureList() {
		return lectures;
	}

	public ObjectProperty<Lecture> selectedProperty() {
		return selected;
	}
}
