package schoolSystem.client.viewmodel;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.SchoolSystemMediator;

public class MyProfileViewModel {

	private StringProperty name;

	private SchoolSystemMediator mediator;
	private User user;

	public MyProfileViewModel(SchoolSystemMediator mediator, User u) {
		this.mediator = mediator;
		this.user = u;

		this.name = new SimpleStringProperty(u.getName());
	}

	public StringProperty usernameProperty() {
		return name;
	}
}
