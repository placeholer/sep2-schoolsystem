package schoolSystem.client.viewmodel;

import java.util.function.Consumer;

import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.ObservableMediator;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.SearchProvider;

public interface ViewModelFactory {

	public CourseListViewModel getCourseListViewModel(User user);

	public MyProfileViewModel getMyProfileViewModel(User user);

	public AddCourseViewModel getAddCourseViewModel(Course course);

	public AddLectureViewModel getAddLectureViewModel(Course c, Lecture l);

	public CreateUserViewModel getCreateUserViewModel(User user);

	public LectureListViewModel getLectureListViewModel(Course course);

	public CalendarViewModel getCalendarViewModel(User user);

	public LoginViewModel getLoginViewModel();

	public UsersListViewModel getUsersListViewModel();

	public <T> SearchViewModel<T> getSearchViewModel(SearchProvider<T, String> provider, Consumer<SearchItem<T>> callback);

	public void setMediator(ObservableMediator mediator);
}