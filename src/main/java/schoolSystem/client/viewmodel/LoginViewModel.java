package schoolSystem.client.viewmodel;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import schoolSystem.client.Client;
import schoolSystem.client.ClientApp;
import schoolSystem.client.ConnectionException;
import schoolSystem.client.RmiClientImpl;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.security.InvalidPasswordException;
import schoolSystem.shared.security.InvalidUsernameException;

public class LoginViewModel {
	private StringProperty server;
	private IntegerProperty port;
	private StringProperty username;
	private StringProperty password;

	private ViewModelFactory vmfactory;

	public LoginViewModel(ViewModelFactory vmfactory) {
		this.vmfactory = vmfactory;

		server = new SimpleStringProperty();
		port = new SimpleIntegerProperty();
		port.set(1099);
		username = new SimpleStringProperty();
		password = new SimpleStringProperty();

		server.set("localhost");
		username.set("test");
		password.set("test");
	}

	public User login() throws ConnectionException, InvalidPasswordException, InvalidUsernameException {
		Client cl = new RmiClientImpl(server.get(), port.get());
		User user = cl.login(username.get(), password.get());

		// if no exception was thrown, the login was successful
		ClientApp.getApp().setClient(cl);
		vmfactory.setMediator(cl.getMediator());

		return user;
	}

	public StringProperty getLoginViewServer() {
		return this.server;
	}

	public IntegerProperty portProperty() {
		return port;
	}

	public StringProperty getLoginViewUsername() {
		return this.username;
	}

	public StringProperty getLoginViewPassword() {
		return this.password;
	}
}
