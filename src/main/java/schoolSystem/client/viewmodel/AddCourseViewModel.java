package schoolSystem.client.viewmodel;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.mediator.SchoolSystemMediator;
import schoolSystem.shared.search.SearchProvider;


public class AddCourseViewModel {

	private SchoolSystemMediator mediator;

	private StringProperty courseName;
	private StringProperty courseDescription;
	private ObservableList<Teacher> teacherList;
	private ObservableList<Student> studentList;

	private Course course;

	public AddCourseViewModel(SchoolSystemMediator mediator) {
		this.mediator = mediator;

		this.courseName = new SimpleStringProperty();
		this.courseDescription = new SimpleStringProperty();
		this.teacherList = FXCollections.observableArrayList();
		this.studentList = FXCollections.observableArrayList();
	}

	public AddCourseViewModel(SchoolSystemMediator mediator, Course course) {
		this(mediator);
		if(course != null)
			setCourse(course);
	}

	public void setCourse(Course course) {
		this.course = course;

		courseName.set(course.getName());
		courseDescription.set(course.getDescription());
		teacherList.setAll(course.getTeachers());
		studentList.setAll(course.getStudents());
	}

	public void save() {
		if(null == course) {
			mediator.addCourse(courseName.get(), courseDescription.get(), new ArrayList<>(teacherList), new ArrayList<>(studentList));
		} else {
			course.setName(courseName.get());
			course.setDescription(courseDescription.get());
			course.setStudents(studentList);
			course.setTeachers(teacherList);
			mediator.updateCourse(course);
		}
	}

	public StringProperty courseNameProperty() {
		return courseName;
	}

	public StringProperty courseDescriptionProperty() {
		return courseDescription;
	}

	public ObservableList<Teacher> observableTeacherList() {
		return teacherList;
	}

	public ObservableList<Student> observableStudentList() {
		return studentList;
	}

	public SearchProvider<Student, String> studentSearchProvider() {
		return mediator.getStudentSearchProvider();
	}

	public SearchProvider<Teacher, String> teacherSearchProvider() {
		return mediator.getTeacherSearchProvider();
	}
}
