package schoolSystem.client.viewmodel;

import java.util.function.Consumer;

import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.ObservableMediator;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.SearchProvider;

public class ViewModelFactoryImpl implements ViewModelFactory {

	private ObservableMediator mediator;

	public ViewModelFactoryImpl(ObservableMediator mediator) {
		this.mediator = mediator;
	}

	public ViewModelFactoryImpl() {}

	public void setMediator(ObservableMediator mediator) {
		this.mediator = mediator;
	}

	@Override
	public LoginViewModel getLoginViewModel() {
		return new LoginViewModel(this);
	}

	@Override
	public CourseListViewModel getCourseListViewModel(User user) {
		if(null == mediator)
			throw new IllegalStateException("Mediator has not been initialized");
		return new CourseListViewModel(mediator, user);
	}

	@Override
	public AddCourseViewModel getAddCourseViewModel(Course course) {
		if(null == mediator)
			throw new IllegalStateException("Mediator has not been initialized");
		return new AddCourseViewModel(mediator, course);
	}

	@Override
	public AddLectureViewModel getAddLectureViewModel(Course c, Lecture l) {
		if(null == mediator)
			throw new IllegalStateException("Mediator has not been initialized");
		return new AddLectureViewModel(mediator, c, l);
	}

	@Override
	public LectureListViewModel getLectureListViewModel(Course course) {
		if(null == mediator)
			throw new IllegalStateException("Mediator has not been initialized");
		return new LectureListViewModel(mediator, course);
	}

	@Override
	public CalendarViewModel getCalendarViewModel(User user) {
		if(null == mediator)
			throw new IllegalStateException("Mediator has not been initialized");
		return new CalendarViewModel(mediator, user);
	}

	@Override
	public UsersListViewModel getUsersListViewModel() {
		if(null == mediator)
			throw new IllegalStateException("Mediator has not been initialized");
		return new UsersListViewModel(mediator);
	}

	@Override
	public <T> SearchViewModel<T> getSearchViewModel(SearchProvider<T, String> provider, Consumer<SearchItem<T>> callback) {
		if(null == mediator)
			throw new IllegalStateException("Mediator has not been initialized");
		return new SearchViewModel<T>(provider, callback);
	}

	@Override
	public CreateUserViewModel getCreateUserViewModel(User user) {
		return new CreateUserViewModel(mediator, user);
	}

	@Override
	public MyProfileViewModel getMyProfileViewModel(User user) {
		return new MyProfileViewModel(mediator, user);
	}
}