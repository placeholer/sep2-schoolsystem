package schoolSystem.client.viewmodel;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Locale;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.ObservableMediator;
import schoolSystem.shared.util.Event;
import schoolSystem.shared.util.Listener;

public class CalendarViewModel {

	private ObservableList<Lecture> firstDay;
	private ObservableList<Lecture> secondDay;
	private ObservableList<Lecture> thirdDay;
	private ObservableList<Lecture> fourthDay;
	private ObservableList<Lecture> fifthDay;
	private ObjectProperty<LocalDate> currentDate;
	private IntegerProperty weekNumber;

	private ObservableMediator mediator;
	private User user;

	public CalendarViewModel(ObservableMediator mediator, User user) {
		this.user = user;
		this.mediator = mediator;
		mediator.addListener(new Listener() {
			@Override
			public <T> void handle(Event<T> e) {
				Platform.runLater(() -> update());
			}
		});

		this.firstDay = FXCollections.observableArrayList();
		this.secondDay = FXCollections.observableArrayList();
		this.thirdDay = FXCollections.observableArrayList();
		this.fourthDay = FXCollections.observableArrayList();
		this.fifthDay = FXCollections.observableArrayList();
		this.currentDate = new SimpleObjectProperty<>();

		currentDate.addListener((obs, o, n) -> update());

		currentDate.set(LocalDate.now());

		this.weekNumber = new SimpleIntegerProperty();
		weekNumber.bind(Bindings.createIntegerBinding(() -> {
			WeekFields weekFields = WeekFields.of(Locale.getDefault());
			return currentDate.get().get(weekFields.weekOfWeekBasedYear());
		}, currentDate));
	}

	private void update() {
		firstDay.setAll(mediator.getLecturesForUserByDate(
					user, dayOfWeek(1)));

		secondDay.setAll(mediator.getLecturesForUserByDate(
					user, dayOfWeek(2)));

		thirdDay.setAll(mediator.getLecturesForUserByDate(
					user, dayOfWeek(3)));

		fourthDay.setAll(mediator.getLecturesForUserByDate(
					user, dayOfWeek(4)));

		fifthDay.setAll(mediator.getLecturesForUserByDate(
					user, dayOfWeek(5)));
	}

	public void back() {
		currentDate.set(currentDate.get().minusWeeks(1));
	}

	public void forward() {
		currentDate.set(currentDate.get().plusWeeks(1));
	}

	public User getUser() {
		return user;
	}

	public ObservableList<Lecture> firstDayList() {
		return firstDay;
	}

	public ObservableList<Lecture> secondDayList() {
		return secondDay;
	}

	public ObservableList<Lecture> thirdDayList() {
		return thirdDay;
	}

	public ObservableList<Lecture> fourthDayList() {
		return fourthDay;
	}

	public ObservableList<Lecture> fifthDayList() {
		return fifthDay;
	}

	public IntegerProperty weekNumberProperty() {
		return weekNumber;
	}

	public ObjectProperty<LocalDate> currentDateProperty() {
		return currentDate;
	}

	public LocalDate getCurrentDate() {
		return currentDate.get();
	}

	private LocalDate dayOfWeek(int day) {
		return currentDate.get().with(DayOfWeek.MONDAY).plusDays(day - 1);
	}
}
