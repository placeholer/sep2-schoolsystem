package schoolSystem.client.viewmodel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.SearchProvider;

public class SearchViewModel<T> {

	private SearchProvider<T, String> searchProvider;

	private StringProperty phrase;
	private ObservableList<SearchItem<T>> results;

	private ExecutorService exe;
	private Future<?> searchTask;
	private Consumer<SearchItem<T>> callback;

	private ObjectProperty<SearchItem<T>> selected;

	public SearchViewModel(SearchProvider<T, String> provider, Consumer<SearchItem<T>> callback) {
		this.searchProvider = provider;
		this.callback = callback;

		results = FXCollections.observableArrayList();
		exe = Executors.newSingleThreadExecutor();

		selected = new SimpleObjectProperty<>();
		phrase = new SimpleStringProperty();
		phrase.addListener((obs, o, n) -> {
			if(searchTask != null)
				searchTask.cancel(true);

			if(n.length() < 2) {
				results.clear();
				return;
			}

			searchTask = exe.submit(() -> {
				Platform.runLater(() ->
						results.setAll(searchProvider.search(n)));
			});
		});
	}

	public StringProperty phraseProperty() {
		return phrase;
	}

	public ObservableList<SearchItem<T>> resultsProperty() {
		return results;
	}

	public ObjectProperty<SearchItem<T>> selectedProperty() {
		return selected;
	}

	public void confirm() {
		if(selected.get() == null && results.size() > 0)
			callback.accept(results.get(0));
		callback.accept(selected.get());
	}
}
