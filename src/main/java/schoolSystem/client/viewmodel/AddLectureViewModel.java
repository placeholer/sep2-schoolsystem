package schoolSystem.client.viewmodel;

import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.mediator.SchoolSystemMediator;
import schoolSystem.shared.search.SearchProvider;

public class AddLectureViewModel {

	private StringProperty subject;
	private StringProperty description;
	private ObjectProperty<LocalDate> date;
	private ObjectProperty<Teacher> teacher;

	private SchoolSystemMediator mediator;
	private Lecture lecture;
	private Course course;

	public AddLectureViewModel(SchoolSystemMediator mediator, Course course, Lecture lecture) {
		this.mediator = mediator;
		this.course = course;

		this.description = new SimpleStringProperty();
		this.subject = new SimpleStringProperty();
		this.date = new SimpleObjectProperty<>(LocalDate.now());
		this.teacher = new SimpleObjectProperty<>();

		if(null != lecture)
			setLecture(lecture);
	}

	public void setLecture(Lecture l) {
		this.lecture = l;

		subject.set(l.getSubject());
		description.set(l.getDescription());
		date.set(l.getDate());
		teacher.set(l.getTeacher());
	}

	public void save() {
		if(null == lecture)
			mediator.addLecture(course, teacher.get(), subject.get(), description.get(), date.get());
		else {
			lecture.setSubject(subject.get());
			lecture.setDescription(description.get());
			lecture.setDate(date.get());
			lecture.setTeacher(teacher.get());

			mediator.updateLecture(lecture);
		}
	}

	public StringProperty subjectProperty() {
		return this.subject;
	}

	public StringProperty descriptionProperty() {
		return this.description;
	}

	public ObjectProperty<LocalDate> dateProperty() {
		return this.date;
	}

	public ObjectProperty<Teacher> teacherProperty() {
		return teacher;
	}

	public SearchProvider<Teacher, String> teacherSearchProvider() {
		return mediator.getTeacherSearchProvider();
	}
}
