package schoolSystem.client;

import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.ObservableMediator;
import schoolSystem.shared.security.InvalidPasswordException;
import schoolSystem.shared.security.InvalidUsernameException;

/**
 * Base interface for Clients
 */
public interface Client {

	/**
	 * Gets SchoolSystemMediator proxy
	 *
	 * @return SchoolSystemMediator
	 */
	public ObservableMediator getMediator() throws ConnectionException;

	/**
	 * Tries to log in to the server using given username and password
	 * 
	 * @param username	the username
	 * @param password	the password
	 * @return the User
	 */
	public User login(String username, String password) throws InvalidPasswordException, InvalidUsernameException, ConnectionException;

	/**
	 * Disconnects client from the server
	 */
	public void disconnect() throws ConnectionException;

	public User getUser();

	public boolean isConnected() throws ConnectionException;

	public boolean isLoggedIn() throws ConnectionException;
}
