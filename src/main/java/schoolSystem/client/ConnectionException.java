package schoolSystem.client;

public class ConnectionException extends Exception {

	private static final long serialVersionUID = 1L;

	public ConnectionException() {}

	public ConnectionException(String message) {
		super(message);
	}

	public ConnectionException(Throwable t) {
		super(t);
	}

	public ConnectionException(String message, Throwable t) {
		super(message, t);
	}
}
