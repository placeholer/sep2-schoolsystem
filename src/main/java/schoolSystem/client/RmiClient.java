package schoolSystem.client;

import java.rmi.RemoteException;
import java.util.UUID;

import schoolSystem.shared.mediator.ObservableRemoteMediatorAdapter;
import schoolSystem.shared.security.InvalidTokenException;

/**
 * Interface for RMI-based clients
 */
public interface RmiClient extends Client {

	/**
	 * Gets token used to authenticate the client
	 * 
	 * @return authentication token
	 */
	public UUID getToken();


	/**
	 * Gets {@link schoolSystem.shared.mediator.ObservableRemoteMediatorAdapter ObservableRemoteMediatorAdapter} RMI stub acquired from RmiServer
	 *
	 * @return ObservableRemoteMediatorAdapter from RmiServer
	 * @throws InvalidTokenException
	 * @throws RemoteException
	 */
	public ObservableRemoteMediatorAdapter getRemoteAdapter() throws RemoteException, InvalidTokenException;
}
