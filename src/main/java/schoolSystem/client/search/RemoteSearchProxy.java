package schoolSystem.client.search;

import java.rmi.RemoteException;
import java.util.List;

import schoolSystem.shared.search.RemoteSearchProvider;
import schoolSystem.shared.search.SearchItem;
import schoolSystem.shared.search.SearchProvider;

public class RemoteSearchProxy<T, V> implements SearchProvider<T, V> {

	private RemoteSearchProvider<T, V> adapter;

	public RemoteSearchProxy(RemoteSearchProvider<T, V> adapter) {
		this.adapter = adapter;
	}

	@Override
	public List<SearchItem<T>> search(V key) {
		try {
			return adapter.search(key);
		} catch (RemoteException e) {
			throw new RuntimeException(e);
		}
	}
}
