package schoolSystem.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import schoolSystem.server.RmiServer;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.mediator.ObservableMediator;
import schoolSystem.shared.mediator.ObservableRemoteMediatorAdapter;
import schoolSystem.shared.mediator.RemoteMediatorProxy;
import schoolSystem.shared.security.InvalidPasswordException;
import schoolSystem.shared.security.InvalidTokenException;
import schoolSystem.shared.security.InvalidUsernameException;

/**
 * Implementation of RMI-based client
 */
public class RmiClientImpl implements RmiClient {

	/**
	 * Name of the server to lookup
	 */
	private static final String SERVER_NAME = "SchoolSystemServer";

	private RmiServer server;
	private ObservableMediator mediator;
	private String host;
	private int port;

	private User user;

	private static final Logger LOG = Logger.getLogger(RmiClientImpl.class.getName());

	/**
	 * Initializes the Client with given address and port
	 *
	 * @param host	address of the host with RMI Registry
	 * @param port	port listening for RMI connenction
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	public RmiClientImpl(String host, int port) throws ConnectionException {
		this.host = host;
		this.port = port;

		LOG.info(String.format("Connecting to server [ %s:%d ]", host, port));

		try {
			Registry reg = LocateRegistry.getRegistry(host, port);
			this.server = (RmiServer) reg.lookup(SERVER_NAME);
		} catch (RemoteException | NotBoundException e) {
			throw new ConnectionException(e);
		}
	}

	public UUID getToken() {
		if(null == user)
			throw new IllegalStateException("Not logged in");
		return user.getAuthToken();
	}

	public User getUser() {
		return user;
	}

	@Override
	public ObservableMediator getMediator() throws ConnectionException {
		if(null == user)
			throw new IllegalStateException("Not logged in! Call login() first");

		if(null == mediator) {
			try {
				mediator = new RemoteMediatorProxy(this);
			} catch (RemoteException | InvalidTokenException e) {
				LOG.log(Level.SEVERE, String.format("Could not connect to server [ %s:%d ]", host, port), e);

				throw new ConnectionException(String.format("Could not connect to server [ %s, %d ]", host, port), e);
			}
		}
		return mediator;
	}

	@Override
	public ObservableRemoteMediatorAdapter getRemoteAdapter() throws RemoteException, InvalidTokenException {
		if(null == server)
			throw new IllegalStateException("Not connected");
		return server.getMediator(getToken());
	}

	@Override
	public User login(String username, String password) throws InvalidUsernameException, InvalidPasswordException, ConnectionException {
		if(null == server)
			throw new IllegalStateException("Not connected");

		try {
			this.user = server.login(username, password);

			LOG.info("Connected");

			return user;
		} catch(InvalidUsernameException | InvalidPasswordException e) {
			LOG.log(Level.WARNING, "Credentials invalid", e);

			throw e;
		} catch (RemoteException e) {
			LOG.log(Level.SEVERE, String.format("Could not connect to server [ %s, %d ]", host, port), e);

			throw new ConnectionException(e);
		}
	}

	@Override
	public boolean isConnected() throws ConnectionException {
		if(null == server)
			return false;

		try {
			server.ping();

			return true;
		} catch (RemoteException e) {
			throw new ConnectionException(e);
		}
	}

	@Override
	public boolean isLoggedIn() throws ConnectionException {
		if(null == user)
			return false;
		return isConnected();
	}

	@Override
	public void disconnect() throws ConnectionException {
		LOG.info(String.format("Disconnecting from [ %s:%d ]", host, port));

		if(null == server) {
			LOG.log(Level.WARNING, "Not connected to any server");

			throw new IllegalStateException("Client not connected");
		}

		try {
			server.logout(getToken());
		} catch (RemoteException | InvalidTokenException e) {
			throw new ConnectionException(e);
		} finally {
			server = null;
		}
	}
}
