package schoolSystem.shared.search;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class RemoteSearchAdapter<T, V> extends UnicastRemoteObject implements RemoteSearchProvider<T, V> {

	private static final long serialVersionUID = 1L;

	private SearchProvider<T, V> provider;

	public RemoteSearchAdapter(SearchProvider<T, V> provider) throws RemoteException {
		this.provider = provider;
	}

	public List<SearchItem<T>> search(V key) throws RemoteException {
		return provider.search(key);
	}
}
