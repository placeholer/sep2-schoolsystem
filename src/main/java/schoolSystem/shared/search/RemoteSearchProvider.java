package schoolSystem.shared.search;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface RemoteSearchProvider<T, V> extends Remote {
	public List<SearchItem<T>> search(V key) throws RemoteException;
}
