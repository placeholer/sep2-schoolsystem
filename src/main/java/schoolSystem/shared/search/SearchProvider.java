package schoolSystem.shared.search;

import java.util.List;

/**
 * Base interface for classes providing search functionality
 */
public interface SearchProvider<T, V> {

	/**
	 * Searches for matching instances of T by key V
	 *
	 * @param key	the criteria to search by
	 * @return list of matching results
	 */
	public List<SearchItem<T>> search(V key);
}
