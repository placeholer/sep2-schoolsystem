package schoolSystem.shared.search;

public class StringComparator {
	public int compare(String s, String target) {
		int res = 0;

		int si = 0;
		for(int i=0; i<target.length(); i++) {
			if(Character.toLowerCase(target.charAt(i)) == Character.toLowerCase(s.charAt(si))) {
				si++;
				if(si == s.length())
					return res;
			} else {
				res++;
			}
		}

		if(si != s.length())
			res = -1;
		return res;
	}
}
