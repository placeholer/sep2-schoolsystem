package schoolSystem.shared.search;

import java.io.Serializable;

public class SearchItem<T> implements Comparable<SearchItem<T>>, Serializable {

	private static final long serialVersionUID = 1L;

	public final T item;
	public final int score;

	public SearchItem(T item, int score) {
		this.item = item;
		this.score = score;
	}

	@Override
	public int compareTo(SearchItem<T> i) {
		return Integer.compare(score, i.score);
	}
}
