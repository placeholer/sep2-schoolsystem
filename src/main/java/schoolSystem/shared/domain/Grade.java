package schoolSystem.shared.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Entity to store information about a Grade
 */
@Entity
public class Grade implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Course the Grade was given for
	 */
	@ManyToOne
	private Course course;

	/**
	 * Teacher giving the Grade
	 */
	@ManyToOne
	private Teacher teacher;

	/**
	 * Student receiving the Grade
	 */
	@ManyToOne
	private Student student;

	/**
	 * Date the Grade was given
	 */
	@Column
	private LocalDate date;

	@Column
	private String comment;

	/**
	 * Grade in danish grading system. Must be one of:
	 *
	 * <ul>
	 *	<li>12</li>
	 *	<li>10</li>
	 *	<li>7</li>
	 *	<li>4</li>
	 *	<li>2</li>
	 *	<li>0</li>
	 *	<li>-3</li>
	 * </ul>
	 */
	@Column
	private int grade;

	public Grade() {}

	/**
	 * Initializes the Grade
	 *
	 * @param course	Course the grade was given for
	 * @param teacher	Teacher giving the Grade
	 * @param student	Student the Grade is given to
	 * @param date	Date the Grade was given
	 * @param grade	the grade
	 */
	public Grade(Course course, Teacher teacher, Student student, LocalDate date, int grade) {
		this.course = course;
		this.teacher = teacher;
		this.student = student;
		this.date = date;
		this.grade = grade;
	}

	/**
	 * Returns the ID of the grade
	 *
	 * @return ID
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Returns the Course which the grade belongs to
	 *
	 * @return the Course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * Returns the teacher that gave the grade
	 *
	 * @return grade
	 */
	public Teacher getTeacher() {
		return teacher;
	}

	/**
	 * Returns the student to whom the grade was given to
	 *
	 * @return student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * Returns the date when the grade was given
	 *
	 * @return date
	 */
	public LocalDate getDate() {
		return date;
	}

	/**
	 * Returns the grade
	 *
	 * @return grade
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * Gets the comment added to the Grade
	 *
	 * @return comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment for the Grade
	 *
	 * @param comment	comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Grade)){
			return false;
		}
		Grade other = (Grade) obj;
		return course.equals(other.course)
			&& teacher.equals(other.teacher)
			&& student.equals(other.student)
			&& date.equals(other.date)
			&& grade == other.grade;
	}

	@Override
	public int hashCode() {
		return Objects.hash(course, teacher, student, date, grade);
	}

	@Override
	public String toString() {
		return String.format("Grade(%d)[%d, %s, %s, %s, %s]",
				grade, id, course.getName(), student.getName(), teacher.getName(), date);
	}
}
