package schoolSystem.shared.domain;

import javax.persistence.Entity;

@Entity
public class Student extends User {

	private static final long serialVersionUID = 1L;

	public Student() {
		super(null);
	}

	public Student(String name) {
		super(name);
	}

	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Student)){
			return false;
		}
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return "Student " + super.toString();
	}
}
