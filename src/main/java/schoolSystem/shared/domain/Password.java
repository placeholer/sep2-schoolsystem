package schoolSystem.shared.domain;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Objects;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Password {

	private static SecureRandom rand;

	/**
	 * Strength of the encryption
	 */
	private static int ITERATIONS = 65536;

	/**
	 * Length of the enctyption key
	 */
	private static int LENGTH = 128;

	/**
	 * Algorythm used for encryption
	 */
	private static String ALGORITHM = "PBKDF2WithHmacSHA1";

	/**
	 * Length of the salt
	 */
	private static int SALT_LENGTH = 8;

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Salt used for encryption
	 */
	@Column
	private byte[] salt;

	/**
	 * Hash of the password string
	 */
	@Column
	private byte[] hash;

	/**
	 * User associated with the password
	 */
	@OneToOne
	private User user;

	public Password() {}

	/**
	 * Initializes Password with given user and password string
	 *
	 * @param user	user assosiated with the password
	 * @param pass	password phrase
	 */
	public Password(User user, String pass) {
		this.user = user;
		this.salt = generateSalt(SALT_LENGTH);
		this.hash = hashPassword(pass, salt);
	}

	/**
	 * Returns hash of the password with given salt
	 *
	 * @param pass	password phrase
	 * @param salt	salt to use
	 * @return byte array containing password's hash with given salt
	 */
	public byte[] hashPassword(String pass, byte[] salt) {
		KeySpec spec = new PBEKeySpec(pass.toCharArray(), salt, ITERATIONS, LENGTH);

		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance(ALGORITHM);
			return factory.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Check if the provided password is correct
	 *
	 * @param pass	password string to check
	 * @return true if password is correct
	 */
	public boolean auth(String pass) {
		return Arrays.equals(hash, hashPassword(pass, salt));
	}

	/**
	 * Generates random salt with given length
	 *
	 * @param len	length of the salt
	 * @return byte arrat with generated salt
	 */
	public byte[] generateSalt(int len) {
		byte[] salt = new byte[len];
		getRandom().nextBytes(salt);
		return salt;
	}

	/**
	 * Initializes SecureRandom instance if not initialized and returns it
	 *
	 * @return SecureRandom instance
	 */
	private SecureRandom getRandom() {
		if(null == rand) {
			synchronized(this) {
				if(null == rand)
					rand = new SecureRandom();
			}
		}
		return rand;
	}

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public byte[] getSalt() {
		return salt;
	}

	public byte[] getHash() {
		return hash;
	}

	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Password)){
			return false;
		}
		Password other = (Password) obj;
		return user.equals(other.user)
			&& Arrays.equals(salt, other.salt)
			&& Arrays.equals(hash, other.hash);
	}

	@Override
	public int hashCode() {
		return Objects.hash(salt, hash, user);
	}

	@Override
	public String toString() {
		return String.format("[%s] %s", Arrays.toString(salt), Arrays.toString(hash));
	}
}
