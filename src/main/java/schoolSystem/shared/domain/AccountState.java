package schoolSystem.shared.domain;

public enum AccountState {
	ACTIVE, LOCKED, REMOVED;
}
