package schoolSystem.shared.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;

/**
 * Base class for Users
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Name of the User
	 */
	@Column
	private String name;

	@Transient
	private UUID authToken;

	@Column
	@Enumerated(EnumType.STRING)
	private AccountState accountState;

	public User(String name) {
		this.name = name;

		this.accountState = AccountState.ACTIVE;
	}

	/**
	 * Returns the id of the user
	 *
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Returns the name of the user
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the user
	 *
	 * @param name	the name of the User
	 */
	public void setName(String name) {
		this.name = name;
	}

	public void setAuthToken(UUID authToken) {
		this.authToken = authToken;
	}

	public UUID getAuthToken() {
		return authToken;
	}

	public boolean isLoggedIn() {
		return null == authToken;
	}

	public AccountState getAccountState() {
		return accountState;
	}

	public void setAccountState(AccountState accountState) {
		this.accountState = accountState;
	}

	@Override
	public boolean equals (Object obj){
		if(!(obj instanceof User)){
			return false;
		}
		User other = (User) obj;
		if(getId() != null && other.getId() != null)
			return getId() == other.getId();
		return name.equals(other.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public String toString() {
		return String.format("[%d, %s]", id, name);
	}
}
