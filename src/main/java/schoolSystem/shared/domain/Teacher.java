package schoolSystem.shared.domain;

import javax.persistence.Entity;

@Entity
public class Teacher extends User {

	private static final long serialVersionUID = 1L;

	public Teacher() {
		super(null);
	}

	public Teacher(String name) {
		super(name);
	}

	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Teacher)){
			return false;
		}
		return super.equals(obj);
	}

	@Override
	public String toString() {
		return "Teacher" + super.toString();
	}
}
