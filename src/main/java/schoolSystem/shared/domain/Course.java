package schoolSystem.shared.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Course implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String name;

	/**
	 * Teachers qualified to teach the course
	 */
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.REMOVE)
	private Set<Teacher> teachers;

	/**
	 * Students attending the course
	 */
	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.REMOVE)
	private Set<Student> students;

	@Column(columnDefinition = "TEXT")
	private String description;

	public Course() {}

	/**
	 * Initializes the Course with given name
	 *
	 * @param name	name of the course
	 */
	public Course(String name) {
		this.name = name;
		this.teachers = new HashSet<>();
		this.students = new HashSet<>();
	}

	public Course(String name, String description) {
		this(name);
		this.description = description;
	}

	/**
	 * Returns the ID of the course
	 *
	 * @return ID
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Returns the name of the course
	 *
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the course
	 *
	 * @param name	the name of the course
	 *
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the teachers teaching the course
	 *
	 * @return teachers teaching the course
	 */
	public Set<Teacher> getTeachers() {
		return Collections.unmodifiableSet(teachers);
	}

	public void setTeachers(Collection<Teacher> teachers) {
		this.teachers = new HashSet<Teacher>(teachers);
	}

	/**
	 * Returns the students in the course
	 *
	 * @return the students in the course
	 */
	public Set<Student> getStudents() {
		return Collections.unmodifiableSet(students);
	}

	/**
	 * Sets the Students attending the Course
	 * 
	 * @param students	Collection of new Students
	 */
	public void setStudents(Collection<Student> students) {
		this.students = new HashSet<Student>(students);
	}

	/**
	 * Enrolls a Student for the Course
	 * 
	 * @param st	Student to enroll
	 */
	public void enrollStudent(Student st) {
		students.add(st);
	}

	/**
	 * Adds Teacher to teach the Course
	 *
	 * @param t	Teacher to add
	 */
	public void addTeacher(Teacher t) {
		teachers.add(t);
	}

	/**
	 * Removes the Student from the Course
	 * 
	 * @param st	Student to remove
	 */
	public void removeStudent(Student st) {
		students.remove(st);
	}

	/**
	 * Removes the Teacher from the Course
	 * 
	 * @param t	Teacher to remove
	 */
	public void removeTeacher(Teacher t) {
		teachers.remove(t);
	}

	/**
	 * Gets description of the Course
	 *
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets description of the Course
	 *
	 * @param description	new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Course)){
			return false;
		}
		Course other = (Course) obj;

		return name.equals(other.name)
			&& teachers.equals(other.teachers)
			&& students.equals(other.students);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, students, teachers);
	}

	@Override
	public String toString() {
		return String.format("Course[%d, %s]", id, name);
	}
}
