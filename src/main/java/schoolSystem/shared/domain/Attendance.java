package schoolSystem.shared.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Entity to store Attendance
 */
@Entity
public class Attendance implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Lecture the Attendance was given at
	 */
	@ManyToOne
	private Lecture lecture;

	/**
	 * Student the Attendance was given to
	 */
	@ManyToOne
	private Student student;

	/**
	 * true if Student was present, false otherwise
	 */
	@Column
	private boolean present;

	public Attendance() {}

	/**
	 * Initializes the Attendance
	 *
	 * @param lecture	Lecture the Attendance was given at
	 * @param student	Student the Attendance was given to
	 * @param present	true if Student was present
	 */
	public Attendance(Lecture lecture, Student student, boolean present) {
		this.lecture = lecture;
		this.student = student;
		this.present = present;
	}

	/**
	 * Returns the ID of the attendance
	 *
	 * @return ID
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Returns the lecture in which the attendance was given
	 *
	 * @return lecture
	 */
	public Lecture getLecture() {
		return lecture;
	}

	/**
	 * Returns the student to whom the attendance was given
	 *
	 * @return student
	 */
	public Student getStudent() {
		return student;
	}

	/**
	 * Returns the presence of the student
	 *
	 * @return presence, true if student was present, false if student was not present
	 */
	public boolean present() {
		return present;
	}

	/**
	 * Sets the presence of the student
	 *
	 * @param present, true if student was present, false if student was not present
	 */
	public void setPresent(boolean present) {
		this.present = present;
	}

	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Attendance)){
			return false;
		}
		Attendance other = (Attendance) obj;
		return lecture.equals(other.lecture)
			&& student.equals(other.student)
			&& present == other.present;
	}

	@Override
	public int hashCode() {
		return Objects.hash(lecture, student, present);
	}

	@Override
	public String toString() {
		return String.format("Attendance(%b)[%d, %s, %s]", present, id, student.getName(), lecture.getSubject());
	}
}
