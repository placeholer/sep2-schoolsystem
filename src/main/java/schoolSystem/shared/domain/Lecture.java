package schoolSystem.shared.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Entity to store information about Lecture
 */
@Entity
public class Lecture implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * Subject of a Lecture
	 */
	@Column
	private String subject;

	/**
	 * Date the Lecture is planned for
	 */
	@Column
	private LocalDate date;

	/**
	 * Teacher giving the Lecture
	 */
	@ManyToOne
	private Teacher teacher;

	/**
	 * Course the lecture belongs to
	 */
	@ManyToOne
	private Course course;

	/**
	 *	Description about the lecture
	 */
	@Column
	private String description;

	public Lecture() {}

	/**
	 * Initializes the Lecture
	 *
	 * @param course	the Course the Lecture belongs to
	 * @param teacher	the Teacher giving the Lecture
	 * @param subject	the subject
	 * @param description	description of the Lecture
	 * @param date	the date the Lecture is planned for
	 */
	public Lecture(Course course, Teacher teacher, String subject, String description, LocalDate date) {
		this(course, teacher, subject, date);

		this.description = description;
	}

	/**
	 * Initializes the Lecture
	 *
	 * @param course	the Course the Lecture belongs to
	 * @param teacher	the Teacher giving the Lecture
	 * @param subject	the subject
	 * @param date	the date the Lecture is planned for
	 */
	public Lecture(Course course, Teacher teacher, String subject, LocalDate date) {
		this.course = course;
		this.teacher = teacher;
		this.subject = subject;
		this.date = date;
	}

	/**
	 * Returns the ID of the lecture
	 *
	 * @return ID
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Returns the course to which the lecture belongs to
	 *
	 * @return course
	 */
	public Course getCourse() {
		return course;
	}

	/**
	 * Returns the teacher giving the lecture
	 *
	 * @return teacher
	 */
	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	/**
	 * Returns the subject of the lecture
	 *
	 * @return subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject of the lecture
	 *
	 * @param subject	the subject of the lecture
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Returns the date of the lecture
	 *
	 * @return date
	 */
	public LocalDate getDate() {
		return date;
	}

	/**
	 * Sets the date of the lecture
	 *
	 * @param date	the date of the lecture
	 */
	public void setDate(LocalDate date) {
		this.date = date;
	}

	/**
	 * Returns the description of the lecture
	 *
	 * @return description
	 */
	public String getDescription(){return description;}

	/**
	 * Sets the description of the lecture
	 *
	 * @param description	the description of the lecture
	 */
	public void setDescription(String description) {this.description = description;}

	@Override
	public boolean equals (Object obj){
		if (!(obj instanceof Lecture)){
			return false;
		}
		Lecture other = (Lecture) obj;
		return course.equals(other.course)
			&& teacher.equals(other.teacher)
			&& subject.equals(other.subject)
			&& date.equals(other.date);
	}

	@Override
	public int hashCode() {
		return Objects.hash(course, teacher, subject, date);
	}

	@Override
	public String toString() {
		return String.format("Lecture(%s)[%d, %s, %s]", subject, id, teacher.getName(), date.toString());
	}
}
