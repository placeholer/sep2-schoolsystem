package schoolSystem.shared.mediator;

import java.time.LocalDate;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import schoolSystem.server.dao.AbstractDAOFactory;
import schoolSystem.shared.domain.Attendance;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Grade;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Password;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchProvider;

/**
 * Implementation of SchoolSystemMediator interface
 */
public class SchoolSystemMediatorImpl implements SchoolSystemMediator {

	private AbstractDAOFactory factory;

	private static final Logger LOG = Logger.getLogger(SchoolSystemMediatorImpl.class.getName());

	public SchoolSystemMediatorImpl(AbstractDAOFactory factory) {
		LOG.info("Initializing mediator...");

		this.factory = factory;
	}

	@Override
	public Grade addGrade(Course course, Teacher teacher, Student student, LocalDate date, int grade) {
		Grade g = new Grade(course, teacher, student, date, grade);
		this.factory.getGradeDAO().persist(g);
		return g;
	}

	@Override
	public Grade removeGrade(Long id) {
		Grade grade = this.factory.getGradeDAO().get(id);
		this.factory.getGradeDAO().delete(grade);
		return grade;
	}

	@Override
	public Course addCourse(String name, List<Teacher> teachers, List<Student> students) {
		Course c = new Course(name);

		this.factory.getCourseDAO().persist(c);
		LOG.info("Adding a new course: " + c);

		return c;
	}

	@Override
	public Course addCourse(String name, String description, List<Teacher> teachers, List<Student> students) {
		Course c = new Course(name, description);
		c.setTeachers(teachers);
		c.setStudents(students);

		LOG.info("Creating a new course: " + c);
		factory.getCourseDAO().persist(c);

		return c;
	}

	@Override
	public Course removeCourse(Long id) {
		Course c = this.factory.getCourseDAO().get(id);

		LOG.info("Removing a course: " + c);
		this.factory.getCourseDAO().delete(c);

		return c;
	}

	@Override
	public Lecture addLecture(Course course, Teacher teacher, String subject, String description, LocalDate date) {
		Lecture l = new Lecture(course, teacher, subject, description, date);

		LOG.info("Creating new lecture: " + l);
		this.factory.getLectureDAO().persist(l);

		return l;
	}

	@Override
	public Lecture removeLecture(Long id) {
		Lecture l = factory.getLectureDAO().get(id);

		LOG.info("Removing the lecture: " + l);
		this.factory.getLectureDAO().delete(this.factory.getLectureDAO().get(id));

		return l;
	}

	@Override
	public Student addStudent(String name, String password) {
		LOG.info("Creating new student: " + name);

		Student s = new Student(name);
		Password p = new Password(s, password);

		this.factory.getStudentDAO().persist(s);
		this.factory.getPasswordDAO().persist(p);

		return s;
	}

	@Override
	public Student removeStudent(Long id) {
		Student st = factory.getStudentDAO().get(id);

		LOG.info("Removing a student: " + st.getName());

		factory.getCourseDAO().getForStudent(st).forEach(c -> {
			c.removeStudent(st);
			factory.getCourseDAO().update(c);
		});

		this.factory.getPasswordDAO().deleteForUser(st);
		this.factory.getStudentDAO().delete(st);

		return st;
	}

	@Override
	public Teacher addTeacher(String name, String password) {
		LOG.info("Creating new teacher: " + name);

		Teacher t = new Teacher(name);
		Password p = new Password(t, password);

		this.factory.getTeacherDAO().persist(t);
		this.factory.getPasswordDAO().persist(p);

		return t;
	}

	@Override
	public Teacher removeTeacher(Long id) {
		Teacher t = factory.getTeacherDAO().get(id);

		LOG.info("Removing teacher " + t.getName());

		factory.getCourseDAO().getForTeacher(t).forEach(c -> {
			c.removeTeacher(t);
			factory.getCourseDAO().update(c);
		});

		this.factory.getPasswordDAO().deleteForUser(t);
		this.factory.getTeacherDAO().delete(t);

		return t;
	}

	@Override
	public Attendance addAttendance(Lecture lecture, Student student, Boolean present) {
		Attendance attendance = new Attendance(lecture, student, present);
		this.factory.getAttendanceDAO().persist(attendance);

		return attendance;
	}

	@Override
	public Attendance removeAttendance(Long id) {
		Attendance attendance = this.factory.getAttendanceDAO().get(id);
		this.factory.getAttendanceDAO().delete(attendance);

		return attendance;
	}

	@Override
	public List<Lecture> getAllLectures() {
		return this.factory.getLectureDAO().getAll();
	}

	@Override
	public List<Course> getAllCourses() {
		return this.factory.getCourseDAO().getAll();
	}

	@Override
	public List<Student> getAllStudents() {
		return this.factory.getStudentDAO().getAll();
	}

	@Override
	public List<Teacher> getAllTeachers() {
		return this.factory.getTeacherDAO().getAll();
	}

	@Override
	public List<Grade> getAllGrades() {
		return this.factory.getGradeDAO().getAll();
	}

	@Override
	public List<Attendance> getAllAttendance() {
		return this.factory.getAttendanceDAO().getAll();
	}

	@Override
	public List<Lecture> getLecturesByTeacher(Teacher teacher) {
		return this.factory.getLectureDAO().getByTeacher(teacher);
	}

	@Override
	public List<Lecture> getLecturesByCourse(Course course) {
		return this.factory.getLectureDAO().getByCourse(course);
	}

	@Override
	public List<Lecture> getLecturesByDate(LocalDate date) {
		return this.factory.getLectureDAO().getByDate(date);
	}

	@Override
	public List<Lecture> getLecturesInRange(LocalDate from, LocalDate to) {
		return this.factory.getLectureDAO().getInRange(from, to);
	}

	@Override
	public List<Course> getCoursesByTeacher(Teacher teacher) {
		return this.factory.getCourseDAO().getForTeacher(teacher);
	}

	@Override
	public List<Course> getCoursesByStudent(Student student) {
		return this.factory.getCourseDAO().getForStudent(student);
	}

	@Override
	public List<Grade> getGradesByStudent(Student student) {
		return this.factory.getGradeDAO().getByStudent(student);
	}

	@Override
	public List<Grade> getGradesByCourse(Course course) {
		return this.factory.getGradeDAO().getByCourse(course);
	}

	@Override
	public List<Grade> getGradesByStudentAndCourse(Student student, Course course) {
		return this.factory.getGradeDAO().getForStudentByCourse(student, course);
	}

	@Override
	public List<Attendance> getAttendanceByStudent(Student student) {
		return this.factory.getAttendanceDAO().getByStudent(student);
	}

	@Override
	public List<Attendance> getAttendanceByCourse(Course course) {
		return this.factory.getAttendanceDAO().getByCourse(course);
	}

	@Override
	public List<Attendance> getAttendanceByLecture(Lecture lecture) {
		return this.factory.getAttendanceDAO().getByLecture(lecture);
	}

	@Override
	public List<Attendance> getAttendanceByStudentAndDate(Student student, LocalDate date) {
		return this.factory.getAttendanceDAO().getForStudentByDate(student, date);
	}

	@Override
	public List<Attendance> getAttendancByStudentAndCourse(Student student, Course course) {
		return this.factory.getAttendanceDAO().getForStudentByCourse(student, course);
	}

	@Override
	public List<Attendance> getAttendanceByStudenAndLecture(Student student, Lecture lecture) {
		return this.factory.getAttendanceDAO().getForStudentByLecture(student, lecture);
	}

	@Override
	public Student getStudent(Long id) {
		return this.factory.getStudentDAO().get(id);
	}

	@Override
	public Teacher getTeacher(Long id) {
		return this.factory.getTeacherDAO().get(id);
	}

	@Override
	public Grade getGrade(Long id) {
		return this.factory.getGradeDAO().get(id);
	}

	@Override
	public Lecture getLecture(Long id) {
		return this.factory.getLectureDAO().get(id);
	}

	@Override
	public Course getCourse(Long id) {
		return this.factory.getCourseDAO().get(id);
	}

	@Override
	public Attendance getAttendance(Long id) {
		return this.factory.getAttendanceDAO().get(id);
	}

	@Override
	public List<Lecture> getLecturesForStudent(Student st) {
		return factory.getLectureDAO().getForStudent(st);
	}

	@Override
	public List<Lecture> getLecturesByCourseForStudent(Course c, Student st) {
		return factory.getLectureDAO().getByCourseForStudent(c, st);
	}

	@Override
	public List<Lecture> getLecturesForStudent(Student st, LocalDate from, LocalDate to) {
		return factory.getLectureDAO().getForStudent(st, from, to);
	}

	@Override
	public Attendance updateAttendance(Attendance attendance) {
		LOG.info("Updating an attendance: " + attendance);

		this.factory.getAttendanceDAO().update(attendance);
		return attendance;
	}

	@Override
	public Course updateCourse(Course course) {
		LOG.info("Updating a course: " + course.getName());

		this.factory.getCourseDAO().update(course);
		return course;
	}

	@Override
	public Grade updateGrade(Grade grade) {
		LOG.info("Updating a grade: " + grade);

		this.factory.getGradeDAO().update(grade);
		return grade;
	}

	@Override
	public Lecture updateLecture(Lecture lecture) {
		LOG.info("Updating a lecture: " + lecture);

		this.factory.getLectureDAO().update(lecture);
		return lecture;
	}

	@Override
	public Student updateStudent(Student student) {
		LOG.info("Updating a student: " + student);

		this.factory.getStudentDAO().update(student);
		return student;
	}

	@Override
	public Teacher updateTeacher(Teacher teacher) {
		LOG.info("Update a teacher: " + teacher);

		this.factory.getTeacherDAO().update(teacher);
		return teacher;
	}

	@Override
	public SearchProvider<Student, String> getStudentSearchProvider() {
		return factory.getStudentDAO();
	}

	@Override
	public SearchProvider<Teacher, String> getTeacherSearchProvider() {
		return factory.getTeacherDAO();
	}

	@Override
	public SearchProvider<Course, String> getCourseSearchProvider() {
		return factory.getCourseDAO();
	}

	@Override
	public SearchProvider<Lecture, String> getLectureSearchProvider() {
		return factory.getLectureDAO();
	}

	@Override
	public List<Lecture> getLecturesForUserByDate(User st, LocalDate date) {
		return factory.getLectureDAO().getForUserByDate(st, date);
	}

	@Override
	public List<User> getAllUsers() {
		List<User> users = factory.getTeacherDAO().getAll().stream()
			.<User>map(t -> t)	// Map Teacher to User
			.collect(Collectors.toList());

		factory.getStudentDAO().getAll().forEach(users::add);

		return users;
	}
}
