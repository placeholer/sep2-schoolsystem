package schoolSystem.shared.mediator;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import schoolSystem.server.security.AuthManager;
import schoolSystem.shared.domain.Attendance;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Grade;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.RemoteSearchAdapter;
import schoolSystem.shared.search.RemoteSearchProvider;
import schoolSystem.shared.security.InvalidTokenException;
import schoolSystem.shared.util.Event;
import schoolSystem.shared.util.EventType;
import schoolSystem.shared.util.RemoteListener;
import schoolSystem.shared.util.RemoteObservableSupport;

public class RemoteSchoolSystemMediatorImpl implements ObservableRemoteMediatorAdapter {

	private SchoolSystemMediator mediator;
	private AuthManager auth;

	private RemoteObservableSupport support;

	public RemoteSchoolSystemMediatorImpl(SchoolSystemMediator mediator, AuthManager auth) {
		this.mediator = mediator;
		this.auth = auth;

		support = new RemoteObservableSupport();
	}

	@Override
	public Grade addGrade(UUID token, Course course, Teacher teacher, Student student, LocalDate date, int grade)
		throws RemoteException {
		Grade g = mediator.addGrade(course, teacher, student, date, grade);

		Event<Grade> e = new Event<Grade>(g, EventType.ADD, auth.getUser(token));
		support.trigger(e);

		return g;
	}

	@Override
	public Grade removeGrade(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Grade g = mediator.removeGrade(id);

		Event<Grade> e = new Event<Grade>(g, EventType.REMOVE, auth.getUser(token));
		support.trigger(e);

		return g;
	}

	@Override
	public Course addCourse(UUID token, String name, List<Teacher> teachers, List<Student> students)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Course c = mediator.addCourse(name, teachers, students);

		Event<Course> e = new Event<Course>(c, EventType.ADD, auth.getUser(token));
		support.trigger(e);

		return c;
	}

	@Override
	public Course addCourse(UUID token, String name, String description, List<Teacher> teachers, List<Student> students)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Course c = mediator.addCourse(name, description, teachers, students);

		Event<Course> e = new Event<Course>(c, EventType.ADD, auth.getUser(token));
		support.trigger(e);

		return c;
	}

	@Override
	public Course removeCourse(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Course c = mediator.removeCourse(id);

		Event<Course> e = new Event<Course>(c, EventType.REMOVE, auth.getUser(token));
		support.trigger(e);

		return c;
	}

	@Override
	public Lecture addLecture(UUID token, Course course, Teacher teacher, String subject, String description, LocalDate date)
		throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Lecture l = mediator.addLecture(course, teacher, subject, description, date);

		Event<Lecture> e = new Event<Lecture>(l, EventType.ADD, auth.getUser(token));
		support.trigger(e);

		return l;
	}

	@Override
	public Lecture removeLecture(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Lecture l = mediator.removeLecture(id);

		Event<Lecture> e = new Event<>(l, EventType.REMOVE, auth.getUser(token));
		support.trigger(e);

		return l;
	}

	@Override
	public Student addStudent(UUID token, String name, String password) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Student s = mediator.addStudent(name, password);

		Event<Student> e = new Event<Student>(s, EventType.ADD, auth.getUser(token));
		support.trigger(e);

		return s;
	}

	@Override
	public Student removeStudent(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Student s = mediator.removeStudent(id);

		Event<Student> e = new Event<Student>(s, EventType.REMOVE, auth.getUser(token));
		support.trigger(e);

		return s;
	}

	@Override
	public Teacher addTeacher(UUID token, String name, String password) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Teacher t = mediator.addTeacher(name, password);

		Event<Teacher> e = new Event<Teacher>(t, EventType.ADD, auth.getUser(token));
		support.trigger(e);

		return t;
	}

	@Override
	public Teacher removeTeacher(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Teacher t = mediator.removeTeacher(id);

		Event<Teacher> e = new Event<Teacher>(t, EventType.REMOVE, auth.getUser(token));
		support.trigger(e);

		return t;
	}

	@Override
	public Attendance addAttendance(UUID token, Lecture lecture, Student student, Boolean present) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Attendance a = mediator.addAttendance(lecture, student, present);

		Event<Attendance> e = new Event<Attendance>(a, EventType.ADD, auth.getUser(token));
		support.trigger(e);

		return a;
	}

	@Override
	public Attendance removeAttendance(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		Attendance a = mediator.removeAttendance(id);

		Event<Attendance> e = new Event<Attendance>(a, EventType.REMOVE, auth.getUser(token));
		support.trigger(e);

		return a;
	}

	@Override
	public List<Lecture> getAllLectures(UUID token) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAllLectures();
	}

	@Override
	public List<Course> getAllCourses(UUID token) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAllCourses();
	}

	@Override
	public List<Student> getAllStudents(UUID token) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAllStudents();
	}

	@Override
	public List<Teacher> getAllTeachers(UUID token) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAllTeachers();
	}

	@Override
	public List<Grade> getAllGrades(UUID token) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAllGrades();
	}

	@Override
	public List<Attendance> getAllAttendance(UUID token) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAllAttendance();
	}

	@Override
	public List<Lecture> getLecturesByTeacher(UUID token, Teacher teacher) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecturesByTeacher(teacher);
	}

	@Override
	public List<Lecture> getLecturesByCourse(UUID token, Course course) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecturesByCourse(course);
	}

	@Override
	public List<Lecture> getLecturesByDate(UUID token, LocalDate date) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecturesByDate(date);
	}

	@Override
	public List<Lecture> getLecturesInRange(UUID token, LocalDate from, LocalDate to) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecturesInRange(from, to);
	}

	@Override
	public List<Course> getCoursesByTeacher(UUID token, Teacher teacher) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getCoursesByTeacher(teacher);
	}

	@Override
	public List<Course> getCoursesByStudent(UUID token, Student student) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getCoursesByStudent(student);
	}

	@Override
	public List<Grade> getGradesByStudent(UUID token, Student student) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getGradesByStudent(student);
	}

	@Override
	public List<Grade> getGradesByCourse(UUID token, Course course) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getGradesByCourse(course);
	}

	@Override
	public List<Grade> getGradesByStudentAndCourse(UUID token, Student student, Course course) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getGradesByStudentAndCourse(student, course);
	}

	@Override
	public List<Attendance> getAttendanceByStudent(UUID token, Student student) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAttendanceByStudent(student);
	}

	@Override
	public List<Attendance> getAttendanceByCourse(UUID token, Course course) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAttendanceByCourse(course);
	}

	@Override
	public List<Attendance> getAttendanceByLecture(UUID token, Lecture lecture) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAttendanceByLecture(lecture);
	}

	@Override
	public List<Attendance> getAttendanceByStudentAndDate(UUID token, Student student, LocalDate date) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAttendanceByStudentAndDate(student, date);
	}

	@Override
	public List<Attendance> getAttendancByStudentAndCourse(UUID token, Student student, Course course) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAttendancByStudentAndCourse(student, course);
	}

	@Override
	public List<Attendance> getAttendanceByStudenAndLecture(UUID token, Student student, Lecture lecture) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAttendanceByStudenAndLecture(student, lecture);
	}

	@Override
	public Student getStudent(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getStudent(id);
	}

	@Override
	public Teacher getTeacher(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getTeacher(id);
	}

	@Override
	public Grade getGrade(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getGrade(id);
	}

	@Override
	public Lecture getLecture(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecture(id);
	}

	@Override
	public Course getCourse(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getCourse(id);
	}

	@Override
	public Attendance getAttendance(UUID token, Long id) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getAttendance(id);
	}

	@Override
	public boolean testToken(UUID token) {
		return auth.validate(token);
	}

	@Override
	public Attendance updateAttendance(UUID token, Attendance attendance) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		mediator.updateAttendance(attendance);

		Event<Attendance> e = new Event<>(attendance, EventType.UPDATE, auth.getUser(token));
		support.trigger(e);

		return attendance;
	}

	@Override
	public Course updateCourse(UUID token, Course course) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		mediator.updateCourse(course);

		Event<Course> e = new Event<>(course, EventType.UPDATE, auth.getUser(token));
		support.trigger(e);

		return course;
	}

	@Override
	public Grade updateGrade(UUID token, Grade grade) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		mediator.updateGrade(grade);

		Event<Grade> e = new Event<>(grade, EventType.UPDATE, auth.getUser(token));
		support.trigger(e);

		return grade;
	}

	@Override
	public Lecture updateLecture(UUID token, Lecture lecture) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		mediator.updateLecture(lecture);

		Event<Lecture> e = new Event<>(lecture, EventType.UPDATE, auth.getUser(token));
		support.trigger(e);

		return lecture;
	}

	@Override
	public Student updateStudent(UUID token, Student student) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		mediator.updateStudent(student);

		Event<Student> e = new Event<>(student, EventType.UPDATE, auth.getUser(token));
		support.trigger(e);

		return student;
	}

	@Override
	public Teacher updateTeacher(UUID token, Teacher teacher) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		mediator.updateTeacher(teacher);

		Event<Teacher> e = new Event<>(teacher, EventType.UPDATE, auth.getUser(token));
		support.trigger(e);

		return teacher;
	}

	@Override
	public List<Lecture> getLecturesForStudent(UUID token, Student st) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecturesForStudent(st);
	}

	@Override
	public List<Lecture> getLecturesByCourseForStudent(UUID token, Course course, Student st)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecturesByCourseForStudent(course, st);
	}

	@Override
	public List<Lecture> getLecturesForStudent(UUID token, Student st, LocalDate from, LocalDate to)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecturesForStudent(st, from, to);
	}

	@Override
	public List<Lecture> getLecturesForUserByDate(UUID token, User user, LocalDate date)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		return mediator.getLecturesForUserByDate(user, date);
	}

	@Override
	public List<User> getAllUsers(UUID token) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		return mediator.getAllUsers();
	}

	public void addListener(UUID token, RemoteListener listener) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		support.addListener(listener);
	}

	public void removeListener(UUID token, RemoteListener listener) throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();
		support.removeListener(listener);
	}

	@Override
	public RemoteSearchProvider<Student, String> getStudentSearchProvider(UUID token)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		return new RemoteSearchAdapter<Student, String>(mediator.getStudentSearchProvider());
	}

	@Override
	public RemoteSearchProvider<Teacher, String> getTeacherSearchProvider(UUID token)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		return new RemoteSearchAdapter<>(mediator.getTeacherSearchProvider());
	}

	@Override
	public RemoteSearchProvider<Course, String> getCourseSearchProvider(UUID token)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		return new RemoteSearchAdapter<>(mediator.getCourseSearchProvider());
	}

	@Override
	public RemoteSearchProvider<Lecture, String> getLectureSearchProvider(UUID token)
			throws RemoteException, InvalidTokenException {
		if(!auth.validate(token))
			throw new InvalidTokenException();

		return new RemoteSearchAdapter<>(mediator.getLectureSearchProvider());
	}
}
