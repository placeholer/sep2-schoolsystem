package schoolSystem.shared.mediator;

import schoolSystem.shared.util.Observable;

public interface ObservableMediator extends SchoolSystemMediator, Observable {}
