package schoolSystem.shared.mediator;

import java.time.LocalDate;
import java.util.List;

import schoolSystem.shared.domain.Attendance;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Grade;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchProvider;

/**
 * SchoolSystemMediator interface. Defines all methods needed for the application
 */
public interface SchoolSystemMediator {

	/**
	 * Add Grade with given arguments
	 *
	 * @param course	Course the grade was given for
	 * @param teacher	Teacher giving the grade
	 * @param student	student receiving the grade
	 * @param date	date the grade was assigned
	 * @param grade	the grade (in danish grading scale)
	 */
	public Grade addGrade(Course course, Teacher teacher, Student student, LocalDate date, int grade);

	/**
	 * Removes the grade with given ID
	 *
	 * @param id	ID of the grade to remove
	 */
	public Grade removeGrade(Long id);

	/**
	 * Adds new Course
	 *
	 * @param name	name of the course
	 * @param teachers	list of Teachers qualified to teach the Course
	 * @param students	list of Students attending the Course
	 */
	public Course addCourse(String name, List<Teacher> teachers, List<Student> students);

	/**
	 * Add new Course with description
	 * 
	 * @param name	name of the Course
	 * @param description	the description of the Course
	 * @param teachers	list of Teachers qualified to teache the Course
	 * @param students	list of Students attending the Course
	 * @return the created Course
	 */
	public Course addCourse(String name, String description, List<Teacher> teachers, List<Student> students);

	/**
	 * Removes the Course with given ID
	 *
	 * @param id	ID of the course to remove
	 */
	public Course removeCourse(Long id);

	/**
	 * Adds new Lecture
	 *
	 * @param subject	the subject of the lecture
	 * @param date	the date the Lecture is planned for
	 * @param teacher	teacher giving the lecture
	 * @param course	Course the Lecture is associated with
	 */
	public Lecture addLecture(Course course, Teacher teacher, String subject, String description, LocalDate date);

	/**
	 * Removes Lecture with given ID
	 *
	 * @param id	ID of the Lecture to remove
	 */
	public Lecture removeLecture(Long id);

	/**
	 * Adds new Student
	 *
	 * @param name	name of the student
	 */
	public Student addStudent(String name, String password);

	/**
	 * Removes the Student
	 *
	 * @param id	ID of the Student to remove
	 */
	public Student removeStudent(Long id);

	/**
	 * Adds new Teacher
	 *
	 * @param name	name of the teacher
	 */
	public Teacher addTeacher(String name, String password);

	/**
	 * Removes the Teacher
	 *
	 * @param id	ID of the teacher to remove
	 */
	public Teacher removeTeacher(Long id);

	/**
	 * Adds new Attendance
	 *
	 * @param lecture	the Lecture the attendance is associated with
	 * @param student	Student the attendance is for
	 * @param present	true if student was present
	 */
	public Attendance addAttendance(Lecture lecture, Student student, Boolean present);

	/**
	 * Removes Attendance
	 *
	 * @param id	ID of the attendance to remove
	 */
	public Attendance removeAttendance(Long id);

	/**
	 * Gets all Lectures
	 *
	 * @return list of all Lectures
	 */
	public List<Lecture> getAllLectures();

	/**
	 * Gets all Courses
	 *
	 * @return list of all Courses
	 */
	public List<Course> getAllCourses();

	/**
	 * Gets all Students
	 *
	 * @return list of all Students
	 */
	public List<Student> getAllStudents();

	/**
	 * Gets all Teachers
	 *
	 * @return list of all Teachers
	 */
	public List<Teacher> getAllTeachers();

	/**
	 * Gets all grades
	 *
	 * @return list of all Grades
	 */
	public List<Grade> getAllGrades();

	/**
	 * Gets all Attendance
	 *
	 * @return list of all the Attendance
	 */
	public List<Attendance> getAllAttendance();

	/**
	 * Gets all Lectures given by the teacher
	 *
	 * @param teacher	Teacher giving the lectures
	 * @return list of Lectures given by teacher
	 */
	public List<Lecture> getLecturesByTeacher(Teacher teacher);

	/**
	 * Gets all Lectures associated with the course
	 *
	 * @param course	Course the lectures are associated with
	 * @return list of Lectures associated with the course
	 */
	public List<Lecture> getLecturesByCourse(Course course);

	/**
	 * Gets Lectures planned on given date
	 *
	 * @param date	date the Lectures are planned for
	 * @return list of Lectures planned for given day
	 */
	public List<Lecture> getLecturesByDate(LocalDate date);

	/**
	 * Gets Lectures planned in between the dates
	 *
	 * @param from	date after which the Lectures are planned
	 * @param to	date before which the Lectures are planned
	 * @return list of Lectures planned in between dates
	 */
	public List<Lecture> getLecturesInRange(LocalDate from, LocalDate to);

	/**
	 * Gets Courses taught by given Teacher
	 *
	 * @param teacher	Teacher qualified to teach the Course
	 * @return list  of Courses the teacher is qualified to teach
	 */
	public List<Course> getCoursesByTeacher(Teacher teacher);

	/**
	 * Gets the Courses the Student attends to
	 *
	 * @param student	student to filter the Courses by
	 * @return list of the Courses the Students attends to
	 */
	public List<Course> getCoursesByStudent(Student student);

	public default List<Course> getCoursesByUser(User user) {
		if(user instanceof Student)
			return getCoursesByStudent((Student) user);
		else if(user instanceof Teacher)
			return getCoursesByTeacher((Teacher) user);
		throw new UnsupportedOperationException("Unsupported user: " + user);
	}

	/**
	 * Gets the Grades given to a Student
	 *
	 * @param student	Student the Grades where given to
	 * @return list of Grades received by the Student
	 */
	public List<Grade> getGradesByStudent(Student student);

	/**
	 * Gets the Grades associated with given Course
	 *
	 * @param course	the Course the Grades are assosiated with
	 * @return list of Grades associated with the Course
	 */
	public List<Grade> getGradesByCourse(Course course);

	/**
	 * Get Grades Student received for a Course
	 *
	 * @param student	Student the Grades were given to
	 * @param course	Course the Grades were given for
	 * @return list of Grades
	 */
	public List<Grade> getGradesByStudentAndCourse(Student student, Course course);

	/**
	 * Gets attendance of a Student
	 *
	 * @param student	Student the Attendence was given to
	 * @return attendance of the Student
	 */
	public List<Attendance> getAttendanceByStudent(Student student);

	/**
	 * Gets attendance for a Course
	 *
	 * @param course	the Course
	 * @return attendance for the Course
	 */
	public List<Attendance> getAttendanceByCourse(Course course);

	/**
	 * Gets attendance for a given Lecture
	 *
	 * @param lecture	the Lecture
	 * @return the attendance for the Lecture
	 */
	public List<Attendance> getAttendanceByLecture(Lecture lecture);

	/**
	 * Gets the attendance for a Student at given date
	 *
	 * @param student	the Student
	 * @param date	the date the attendance was given for
	 * @return attendance of a Student at given date
	 */
	public List<Attendance> getAttendanceByStudentAndDate(Student student, LocalDate date);

	/**
	 * Gets attendance of a Student for a Course
	 *
	 * @param student	the Student
	 * @param course	the Course
	 * @return attendance of a Student for a Course
	 */
	public List<Attendance> getAttendancByStudentAndCourse(Student student, Course course);

	/**
	 * Gets attendance of a Student for a Lecture
	 *
	 * @param student	the Student
	 * @param lecture	the Lecture
	 * @return attendance for the Student at the Lecture
	 */
	public List<Attendance> getAttendanceByStudenAndLecture(Student student, Lecture lecture);

	/**
	 * Gets the Student by ID
	 *
	 * @param id	ID of the Student
	 * @return the Student or null
	 */
	public Student getStudent(Long id);

	/**
	 * Gets the Teacher by ID
	 *
	 * @param id	ID of the Teacher
	 * @return Teacher or null
	 */
	public Teacher getTeacher(Long id);

	/**
	 * Gets Grade by ID
	 *
	 * @param id	ID of the Grade
	 * @return Grade or null
	 */
	public Grade getGrade(Long id);

	/**
	 * Gets Lecture by ID
	 *
	 * @param id	ID of the Lecture
	 * @return Lecture or null
	 */
	public Lecture getLecture(Long id);

	/**
	 * Gets Course by ID
	 *
	 * @param id	ID of the Course
	 * @return Course or null
	 */
	public Course getCourse(Long id);

	/**
	 * Gets Attendance by ID
	 *
	 * @param id	ID of the Attendance
	 * @return Attendance or null
	 */
	public Attendance getAttendance(Long id);

	/**
	 * Gets Lectures for a Student
	 *
	 * @param 	st	the Student
	 * @return list of Lectures
	 */
	public List<Lecture> getLecturesForStudent(Student st);

	/**
	 * Gets Lectures for a given Course the Student attends
	 *
	 * @param c	the Course
	 * @param st	the Student
	 * @return list of Lectures
	 */
	public List<Lecture> getLecturesByCourseForStudent(Course c, Student st);

	/**
	 * Gets Lectures for a Student planned between the dates (both inclusive)
	 *
	 * @param 	st	the Student
	 * @param from	beginning of the date range
	 * @param to	end of the date range
	 * @return list of Lectures
	 */
	public List<Lecture> getLecturesForStudent(Student st, LocalDate from, LocalDate to);

	public Attendance updateAttendance (Attendance attendance);

	public Course updateCourse (Course course);

	public Grade updateGrade (Grade grade);

	public Lecture updateLecture (Lecture lecture);

	public Student updateStudent (Student student);

	public Teacher updateTeacher (Teacher teacher);

	public SearchProvider<Student, String> getStudentSearchProvider();

	public SearchProvider<Teacher, String> getTeacherSearchProvider();

	public SearchProvider<Course, String> getCourseSearchProvider();

	public SearchProvider<Lecture, String> getLectureSearchProvider();

	public List<Lecture> getLecturesForUserByDate(User user, LocalDate date);

	public List<User> getAllUsers();
}
