package schoolSystem.shared.mediator;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import schoolSystem.client.RmiClient;
import schoolSystem.client.search.RemoteSearchProxy;
import schoolSystem.shared.domain.Attendance;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Grade;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.domain.User;
import schoolSystem.shared.search.SearchProvider;
import schoolSystem.shared.security.InvalidTokenException;
import schoolSystem.shared.util.Listener;
import schoolSystem.shared.util.RemoteListener;
import schoolSystem.shared.util.RemoteListenerAdapter;

public class RemoteMediatorProxy implements ObservableMediator {

	private RmiClient client;
	private ObservableRemoteMediatorAdapter rmiAdapter;

	private List<RemoteListener> listeners;

	public RemoteMediatorProxy(RmiClient client) throws RemoteException, InvalidTokenException {
		this.client = client;
		this.rmiAdapter = client.getRemoteAdapter();

		this.listeners = new LinkedList<>();
	}

	@Override
	public Grade addGrade(Course course, Teacher teacher, Student student, LocalDate date, int grade) {
		try {
			return rmiAdapter.addGrade(client.getToken(), course, teacher, student, date, grade);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Grade removeGrade(Long id) {
		try {
			return rmiAdapter.removeGrade(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Course addCourse(String name, List<Teacher> teachers, List<Student> students) {
		try {
			return rmiAdapter.addCourse(client.getToken(), name, teachers, students);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Course addCourse(String name, String description, List<Teacher> teachers, List<Student> students) {
		try {
			return rmiAdapter.addCourse(client.getToken(), name, description, teachers, students);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Course removeCourse(Long id) {
		try {
			return rmiAdapter.removeCourse(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Lecture addLecture(Course course, Teacher teacher, String subject, String description, LocalDate date) {
		try {
			return rmiAdapter.addLecture(client.getToken(),  course,  teacher,  subject,  description,  date);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Lecture removeLecture(Long id) {
		try {
			return rmiAdapter.removeLecture(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Student addStudent(String name, String password) {
		try {
			return rmiAdapter.addStudent(client.getToken(), name, password);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Student removeStudent(Long id) {
		try {
			return rmiAdapter.removeStudent(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Teacher addTeacher(String name, String password) {
		try {
			return rmiAdapter.addTeacher(client.getToken(), name, password);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Teacher removeTeacher(Long id) {
		try {
			return rmiAdapter.removeTeacher(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Attendance addAttendance(Lecture lecture, Student student, Boolean present) {
		try {
			return rmiAdapter.addAttendance(client.getToken(), lecture, student, present);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Attendance removeAttendance(Long id) {
		try {
			return rmiAdapter.removeAttendance(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getAllLectures() {
		try {
			return rmiAdapter.getAllLectures(client.getToken());
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Course> getAllCourses() {
		try {
			return rmiAdapter.getAllCourses(client.getToken());
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Student> getAllStudents() {
		try {
			return rmiAdapter.getAllStudents(client.getToken());
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Teacher> getAllTeachers() {
		try {
			return rmiAdapter.getAllTeachers(client.getToken());
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Grade> getAllGrades() {
		try {
			return rmiAdapter.getAllGrades(client.getToken());
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Attendance> getAllAttendance() {
		try {
			return rmiAdapter.getAllAttendance(client.getToken());
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getLecturesByTeacher(Teacher teacher) {
		try {
			return rmiAdapter.getLecturesByTeacher(client.getToken(), teacher);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getLecturesByCourse(Course course) {
		try {
			return rmiAdapter.getLecturesByCourse(client.getToken(), course);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getLecturesByDate(LocalDate date) {
		try {
			return rmiAdapter.getLecturesByDate(client.getToken(), date);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getLecturesInRange(LocalDate from, LocalDate to) {
		try {
			return rmiAdapter.getLecturesInRange(client.getToken(), from, to);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Course> getCoursesByTeacher(Teacher teacher) {
		try {
			return rmiAdapter.getCoursesByTeacher(client.getToken(), teacher);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Course> getCoursesByStudent(Student student) {
		try {
			return rmiAdapter.getCoursesByStudent(client.getToken(), student);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Grade> getGradesByStudent(Student student) {
		try {
			return rmiAdapter.getGradesByStudent(client.getToken(), student);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Grade> getGradesByCourse(Course course) {
		try {
			return rmiAdapter.getGradesByCourse(client.getToken(), course);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Grade> getGradesByStudentAndCourse(Student student, Course course) {
		try {
			return rmiAdapter.getGradesByStudentAndCourse(client.getToken(), student, course);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Attendance> getAttendanceByStudent(Student student) {
		try {
			return rmiAdapter.getAttendanceByStudent(client.getToken(), student);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Attendance> getAttendanceByCourse(Course course) {
		try {
			return rmiAdapter.getAttendanceByCourse(client.getToken(), course);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Attendance> getAttendanceByLecture(Lecture lecture) {
		try {
			return rmiAdapter.getAttendanceByLecture(client.getToken(), lecture);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Attendance> getAttendanceByStudentAndDate(Student student, LocalDate date) {
		try {
			return rmiAdapter.getAttendanceByStudentAndDate(client.getToken(), student, date);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Attendance> getAttendancByStudentAndCourse(Student student, Course course) {
		try {
			return rmiAdapter.getAttendancByStudentAndCourse(client.getToken(), student, course);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Attendance> getAttendanceByStudenAndLecture(Student student, Lecture lecture) {
		try {
			return rmiAdapter.getAttendanceByStudenAndLecture(client.getToken(), student, lecture);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Student getStudent(Long id) {
		try {
			return rmiAdapter.getStudent(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Teacher getTeacher(Long id) {
		try {
			return rmiAdapter.getTeacher(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Grade getGrade(Long id) {
		try {
			return rmiAdapter.getGrade(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Lecture getLecture(Long id) {
		try {
			return rmiAdapter.getLecture(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Course getCourse(Long id) {
		try {
			return rmiAdapter.getCourse(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Attendance getAttendance(Long id) {
		try {
			return rmiAdapter.getAttendance(client.getToken(), id);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getLecturesForStudent(Student st) {
		try {
			return rmiAdapter.getLecturesForStudent(client.getToken(), st);
		} catch(RemoteException | InvalidTokenException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getLecturesByCourseForStudent(Course c, Student st) {
		try {
			return rmiAdapter.getLecturesByCourseForStudent(client.getToken(), c, st);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getLecturesForStudent(Student st, LocalDate from, LocalDate to) {
		try {
			return rmiAdapter.getLecturesForStudent(client.getToken(), st, from, to);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<Lecture> getLecturesForUserByDate(User user, LocalDate date) {
		try {
			return rmiAdapter.getLecturesForUserByDate(client.getToken(), user, date);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Attendance updateAttendance(Attendance attendance) {
		try {
			return rmiAdapter.updateAttendance(client.getToken(), attendance);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Course updateCourse(Course course) {
		try {
			return rmiAdapter.updateCourse(client.getToken(), course);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Grade updateGrade(Grade grade) {
		try {
			return rmiAdapter.updateGrade(client.getToken(), grade);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Lecture updateLecture(Lecture lecture) {
		try {
			return rmiAdapter.updateLecture(client.getToken(), lecture);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Student updateStudent(Student student) {
		try {
			return rmiAdapter.updateStudent(client.getToken(), student);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Teacher updateTeacher(Teacher teacher) {
		try {
			return rmiAdapter.updateTeacher(client.getToken(), teacher);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<User> getAllUsers() {
		try {
			return rmiAdapter.getAllUsers(client.getToken());
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void addListener(Listener l) {
		try {
			RemoteListener rl = (RemoteListener) UnicastRemoteObject.exportObject(new RemoteListenerAdapter(l), 0);
			listeners.add(rl);
			rmiAdapter.addListener(client.getToken(), rl);
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeListener(Listener l) {
		Optional<RemoteListener> res = listeners.stream()
			.filter(rl -> {
				try {
					return rl.getListener().equals(l);
				} catch(RemoteException e) {
					throw new RuntimeException(e);
				}
			})
			.findAny();

		if(res.isEmpty())
			throw new IllegalArgumentException("No such listener");

		try {
			rmiAdapter.removeListener(client.getToken(), res.get());
		} catch (RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeListeners() {
		listeners.forEach(l -> {
			try {
				rmiAdapter.removeListener(client.getToken(), l);
			} catch (RemoteException | InvalidTokenException e) {
				throw new RuntimeException(e);
			}
		});
	}

	@Override
	public SearchProvider<Student, String> getStudentSearchProvider() {
		try {
			return new RemoteSearchProxy<Student, String>(rmiAdapter.getStudentSearchProvider(client.getToken()));
		} catch(RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public SearchProvider<Teacher, String> getTeacherSearchProvider() {
		try {
			return new RemoteSearchProxy<Teacher, String>(rmiAdapter.getTeacherSearchProvider(client.getToken()));
		} catch(RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public SearchProvider<Course, String> getCourseSearchProvider() {
		try {
			return new RemoteSearchProxy<Course, String>(rmiAdapter.getCourseSearchProvider(client.getToken()));
		} catch(RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public SearchProvider<Lecture, String> getLectureSearchProvider() {
		try {
			return new RemoteSearchProxy<Lecture, String>(rmiAdapter.getLectureSearchProvider(client.getToken()));
		} catch(RemoteException | InvalidTokenException e) {
			throw new RuntimeException(e);
		}
	}
}
