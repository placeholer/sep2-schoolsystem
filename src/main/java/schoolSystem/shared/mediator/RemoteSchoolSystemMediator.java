package schoolSystem.shared.mediator;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import schoolSystem.shared.domain.Attendance;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Grade;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;
import schoolSystem.shared.security.InvalidTokenException;

/**
 * Adapter class for SchoolSystemMediator implementing Remote interface
 */
public interface RemoteSchoolSystemMediator extends Remote {

	public void addGrade(UUID token, Course course, Teacher teacher, Student student, LocalDate date, int grade) throws RemoteException, InvalidTokenException;

	public void removeGrade(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public void addCourse(UUID token, String name, List<Teacher> teachers, List<Student> students) throws RemoteException, InvalidTokenException;

	public void removeCourse(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public void addLecture(UUID token, Course course, Teacher teacher, String subject, String description, LocalDate date) throws RemoteException, InvalidTokenException;

	public void removeLecture(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public void addStudent(UUID token, String name, String password) throws RemoteException, InvalidTokenException;

	public void removeStudent(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public void addTeacher(UUID token, String name, String password) throws RemoteException, InvalidTokenException;

	public void removeTeacher(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public void addAttendance(UUID token, Lecture lecture, Student student, Boolean present) throws RemoteException, InvalidTokenException;

	public void removeAttendance(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public List<Lecture> getAllLectures(UUID token) throws RemoteException, InvalidTokenException;

	public List<Course> getAllCourses(UUID token) throws RemoteException, InvalidTokenException;

	public List<Student> getAllStudents(UUID token) throws RemoteException, InvalidTokenException;

	public List<Teacher> getAllTeachers(UUID token) throws RemoteException, InvalidTokenException;

	public List<Grade> getAllGrades(UUID token) throws RemoteException, InvalidTokenException;

	public List<Attendance> getAllAttendance(UUID token) throws RemoteException, InvalidTokenException;

	public List<Lecture> getLecturesByTeacher(UUID token, Teacher teacher) throws RemoteException, InvalidTokenException;

	public List<Lecture> getLecturesByCourse(UUID token, Course course) throws RemoteException, InvalidTokenException;

	public List<Lecture> getLecturesByDate(UUID token, LocalDate date) throws RemoteException, InvalidTokenException;

	public List<Lecture> getLecturesInRange(UUID token, LocalDate from, LocalDate to) throws RemoteException, InvalidTokenException;

	public List<Course> getCoursesByTeacher(UUID token, Teacher teacher) throws RemoteException, InvalidTokenException;

	public List<Course> getCoursesByStudent(UUID token, Student student) throws RemoteException, InvalidTokenException;

	public List<Grade> getGradesByStudent(UUID token, Student student) throws RemoteException, InvalidTokenException;

	public List<Grade> getGradesByCourse(UUID token, Course course) throws RemoteException, InvalidTokenException;

	public List<Grade> getGradesByStudentAndCourse(UUID token, Student student, Course course) throws RemoteException, InvalidTokenException;

	public List<Attendance> getAttendanceByStudent(UUID token, Student student) throws RemoteException, InvalidTokenException;

	public List<Attendance> getAttendanceByCourse(UUID token, Course course) throws RemoteException, InvalidTokenException;

	public List<Attendance> getAttendanceByLecture(UUID token, Lecture lecture) throws RemoteException, InvalidTokenException;

	public List<Attendance> getAttendanceByStudentAndDate(UUID token, Student student, LocalDate date) throws RemoteException, InvalidTokenException;

	public List<Attendance> getAttendancByStudentAndCourse(UUID token, Student student, Course course) throws RemoteException, InvalidTokenException;

	public List<Attendance> getAttendanceByStudenAndLecture(UUID token, Student student, Lecture lecture) throws RemoteException, InvalidTokenException;

	public Student getStudent(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public Teacher getTeacher(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public Grade getGrade(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public Lecture getLecture(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public Course getCourse(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public Attendance getAttendance(UUID token, Long id) throws RemoteException, InvalidTokenException;

	public boolean testToken(UUID token) throws RemoteException;
}
