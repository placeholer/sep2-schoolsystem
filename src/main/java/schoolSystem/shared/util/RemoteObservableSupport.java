package schoolSystem.shared.util;

import java.rmi.RemoteException;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class RemoteObservableSupport implements RemoteObservableInterface {
	private Queue<RemoteListener> listeners;

	public RemoteObservableSupport() {
		listeners = new LinkedBlockingQueue<>();
	}

	@Override
	public void addListener(RemoteListener listener) throws RemoteException {
		listeners.add(listener);
	}

	@Override
	public void removeListener(RemoteListener listener) throws RemoteException {
		listeners.remove(listener);
	}

	public <T> void trigger(Event<T> event){
		listeners.forEach(l -> {
			try {
				l.handle(event);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
	}
}
