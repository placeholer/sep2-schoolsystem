package schoolSystem.shared.util;

public enum EventType {
	ADD, REMOVE, UPDATE;
}
