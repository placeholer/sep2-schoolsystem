package schoolSystem.shared.util;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Remote adapter for Listener interface
 */
public interface RemoteListener extends Remote {
	public <T> void handle(Event<T> event) throws RemoteException;
	public Listener getListener() throws RemoteException;
}
