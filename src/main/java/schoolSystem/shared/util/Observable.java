package schoolSystem.shared.util;

/**
 * Base interface for observable classes
 */
public interface Observable {

	/**
	 * Adds the listener
	 *
	 * @param l	Listener to add
	 */
	public void addListener(Listener l);

	/**
	 * Removes the listener
	 *
	 * @param l	Listener to remove
	 */
	public void removeListener(Listener l);

	/**
	 * Removes all the registered listeners
	 */
	public void removeListeners();
}
