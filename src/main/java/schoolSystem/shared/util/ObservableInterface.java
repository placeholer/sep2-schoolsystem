package schoolSystem.shared.util;

import java.beans.PropertyChangeListener;

public interface ObservableInterface {
    public void addListener(Listener listener);
    public void removeListener(Listener listener);
}
