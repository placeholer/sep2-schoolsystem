package schoolSystem.shared.util;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Observable interface adapter for rmi
 */
public interface RemoteObservableInterface extends Remote {

	/**
	 * Adds remote listener
	 * 
	 * @param listener	remote listener
	 */
	public void addListener(RemoteListener listener) throws RemoteException;

	/**
	 * Removes the remote listener
	 * 
	 * @param listener	remote listener
	 */
	public void removeListener(RemoteListener listener) throws RemoteException;
}
