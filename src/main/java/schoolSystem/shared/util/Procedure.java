package schoolSystem.shared.util;

/**
 * Functional interface for a procedure - function that takes no arguments and returns no value
 */
@FunctionalInterface
public interface Procedure {

	/**
	 * Function to implement
	 */
	public void exec();
}
