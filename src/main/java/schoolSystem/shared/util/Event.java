package schoolSystem.shared.util;

import java.io.Serializable;
import java.time.LocalDate;

import schoolSystem.shared.domain.User;

public class Event<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private T changed;
	private User user;
	private LocalDate date;
	private EventType type;

	public Event(T changed, EventType type, User user, LocalDate date) {
		this.changed = changed;
		this.type = type;
		this.user = user;
		this.date = date;
	}

	public Event(T changed, EventType type, User user) {
		this(changed, type, user, LocalDate.now());
	}

	public T getChanged() {
		return changed;
	}

	public EventType getType() {
		return type;
	}

	public LocalDate getDate() {
		return date;
	}

	public User getUser() {
		return user;
	}

	public String toString() {
		return String.format("EVENT[%s, %s, %s, %s]", date, user.getName(), type, changed);
	}
}
