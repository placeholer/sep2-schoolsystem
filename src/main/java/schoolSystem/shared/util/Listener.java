package schoolSystem.shared.util;

public interface Listener {
    public <T> void handle(Event<T> event);
}
