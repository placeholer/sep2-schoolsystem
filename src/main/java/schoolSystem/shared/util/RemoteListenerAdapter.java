package schoolSystem.shared.util;

import java.rmi.RemoteException;

public class RemoteListenerAdapter implements RemoteListener {

	private Listener listener;

	public RemoteListenerAdapter(Listener listener) {
		this.listener = listener;
	}

	@Override
	public <T> void handle(Event<T> event) throws RemoteException {
		listener.handle(event);
	}

	@Override
	public Listener getListener() throws RemoteException {
		return listener;
	}
}
