package schoolSystem.shared.security;

public class InvalidUsernameException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidUsernameException(String message) {
		super(message);
	}

	public InvalidUsernameException() {
		super("Username is invalid");
	}

	public InvalidUsernameException(Throwable t) {
		super(t);
	}
}
