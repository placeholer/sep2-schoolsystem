package schoolSystem.shared.security;

public class InvalidPasswordException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidPasswordException(String message) {
		super(message);
	}

	public InvalidPasswordException(Throwable t) {
		super(t);
	}

	public InvalidPasswordException() {
		super("The password is invalid");
	}
}
