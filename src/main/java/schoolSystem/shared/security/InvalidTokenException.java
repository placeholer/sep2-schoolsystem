package schoolSystem.shared.security;

public class InvalidTokenException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidTokenException() {
		super("Provided auth token is invalid");
	}

	public InvalidTokenException(String message) {
		super(message);
	}

	public InvalidTokenException(Throwable t) {
		super(t);
	}
}
