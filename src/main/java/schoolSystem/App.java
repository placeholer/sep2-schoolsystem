package schoolSystem;

import javafx.application.Application;
import schoolSystem.server.ServerApp;

public class App {

	public static void main(String[] args) {
		// May be need to set server IP
		// System.setProperty("java.rmi.server.hostname", "localhost");

		if(args.length > 0 && args[0].equals("server")) {
			ServerApp server = new ServerApp();

			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				server.shutdown();
			}));

			server.createTestData();

			server.start();
		} else
			Application.launch(schoolSystem.client.ClientApp.class, args);
	}
}
