package schoolSystem.server.dao.hibernate;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;

public class HibernateLectureTest {

	private static HibernateDAOFactory daoFactory;

	private Course c;
	private Teacher t;
	private Student s;
	private Lecture l;

	@BeforeAll
	public static void setup() {
		daoFactory = DAOFactoryProducer.newHibernateDAOFactory();
	}

	@BeforeEach
	public void initAndPersist() {
		c = new Course("LT course");
		t = new Teacher("LT teacher");
		s = new Student("LT student");
		l = new Lecture(c, t, "LT lecture", LocalDate.now());

		c.enrollStudent(s);

		daoFactory.getStudentDAO().persist(s);
		daoFactory.getCourseDAO().persist(c);
		daoFactory.getTeacherDAO().persist(t);
		daoFactory.getLectureDAO().persist(l);
	}

	@Test
	public void testLectureGetByCourse() throws Exception {
		List<Lecture> res = daoFactory.getLectureDAO().getByCourse(c);

		assertTrue(res.contains(l));
	}

	@Test
	public void testLectureGetByDate() throws Exception {
		List<Lecture> res = daoFactory.getLectureDAO().getByDate(l.getDate());

		assertTrue(res.contains(l));
	}

	@Test
	public void testLectureGetInRange() throws Exception {
		List<Lecture> res = daoFactory.getLectureDAO().getInRange(l.getDate().minusDays(1), l.getDate().plusDays(1));

		assertTrue(res.contains(l));
	}

	@Test
	public void testLectureGetByTeacher() throws Exception {
		List<Lecture> res = daoFactory.getLectureDAO().getByTeacher(t);

		assertTrue(res.contains(l));
	}

	@Test
	public void testLectureGetByCourseForStudent() throws Exception {
		List<Lecture> res = daoFactory.getLectureDAO().getByCourseForStudent(c, s);

		assertTrue(res.contains(l));
	}

	@Test
	public void testLectureGetForStudent() throws Exception {
		List<Lecture> res = daoFactory.getLectureDAO().getForStudent(s);

		assertTrue(res.contains(l));
	}

	@Test
	public void testlectureGetForStudentBetweenDates() throws Exception {
		List<Lecture> res = daoFactory.getLectureDAO().getForStudent(s, l.getDate(), l.getDate());

		assertTrue(res.contains(l));
	}

	@AfterAll
	public static void cleanup() {
		daoFactory.close();
	}
}
