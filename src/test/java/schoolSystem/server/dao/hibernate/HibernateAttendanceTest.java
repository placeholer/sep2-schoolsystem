package schoolSystem.server.dao.hibernate;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.shared.domain.Attendance;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Lecture;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;

public class HibernateAttendanceTest {

	private static HibernateDAOFactory daoFactory;

	private Course c;
	private Teacher t;
	private Lecture l;
	private Student s;
	private Attendance a;

	@BeforeAll
	public static void setup() {
		daoFactory = DAOFactoryProducer.newHibernateDAOFactory();
	}

	@BeforeEach
	public void initializeAndPersist() {
		c = new Course("Hibernate Attendance Test Course");
		t = new Teacher("Hibernate Attendance Test Teacher");
		l = new Lecture(c, t, "Hibernate Attendance Test Lecture", LocalDate.now());
		s = new Student("Hibernate Attendance Test Student");
		a = new Attendance(l, s, true);

		daoFactory.getCourseDAO().persist(c);
		daoFactory.getTeacherDAO().persist(t);
		daoFactory.getLectureDAO().persist(l);
		daoFactory.getStudentDAO().persist(s);
		daoFactory.getAttendanceDAO().persist(a);
	}

	@Test
	public void testAttendanceGetByStudent() throws Exception {
		List<Attendance> res = daoFactory.getAttendanceDAO().getByStudent(s);

		assertTrue(res.contains(a));
	}

	@Test
	public void testAttendanceGetByCourse() throws Exception {
		List<Attendance> res = daoFactory.getAttendanceDAO().getByCourse(c);

		assertTrue(res.contains(a));
	}

	@Test
	public void testAttendanceGetByLecture() throws Exception {
		List<Attendance> res = daoFactory.getAttendanceDAO().getByLecture(l);

		assertTrue(res.contains(a));
	}

	@Test
	public void testAttendanceGetForStudentByDate() throws Exception {
		List<Attendance> res = daoFactory.getAttendanceDAO().getForStudentByDate(s, l.getDate());

		assertTrue(res.contains(a));
	}

	@Test
	public void testAttendanceGetForStudentByCourse() throws Exception {
		List<Attendance> res = daoFactory.getAttendanceDAO().getForStudentByCourse(s, c);

		assertTrue(res.contains(a));
	}

	@Test
	public void testAttendanceGetForStudentByLecture() throws Exception {
		List<Attendance> res = daoFactory.getAttendanceDAO().getForStudentByLecture(s, l);

		assertTrue(res.contains(a));
	}

	@AfterAll
	public static void cleanup() {
		daoFactory.close();
	}
}
