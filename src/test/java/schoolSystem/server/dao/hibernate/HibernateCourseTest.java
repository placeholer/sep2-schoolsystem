package schoolSystem.server.dao.hibernate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;

public class HibernateCourseTest {

	private static HibernateDAOFactory daoFactory;

	@BeforeAll
	public static void setup() {
		daoFactory = DAOFactoryProducer.newHibernateDAOFactory();
	}

	@Test
	public void testCoursePersist() throws Exception {
		Course c = new Course("CP test");

		assertNull(c.getId());

		daoFactory.getCourseDAO().persist(c);

		assertNotNull(c.getId());

		System.out.println(c);

		assertEquals(c, daoFactory.getCourseDAO().get(c.getId()));
	}

	@Test
	public void testCourseDelete() throws Exception {
		Course c = new Course("CD test");

		daoFactory.getCourseDAO().persist(c);

		assertNotNull(c.getId());

		daoFactory.getCourseDAO().delete(c);

		assertNull(daoFactory.getCourseDAO().get(c.getId()));
	}

	@Test
	public void testCourseUpdate() throws Exception {
		Course c = new Course("CU test");

		daoFactory.getCourseDAO().persist(c);

		c.setName("CU test - updated");

		daoFactory.getCourseDAO().update(c);

		assertEquals(daoFactory.getCourseDAO().get(c.getId()).getName(), c.getName());
	}

	@Test
	public void testCourseGetForTeacher() throws Exception {
		Course c = new Course("test get for teacher");
		Teacher t = new Teacher("CGFT");
		c.addTeacher(t);

		daoFactory.getTeacherDAO().persist(t);
		daoFactory.getCourseDAO().persist(c);

		List<Course> res = daoFactory.getCourseDAO().getForTeacher(t);

		System.out.println("################################################");
		System.out.println(c);
		System.out.println(res);

		assertTrue(res.contains(c));
	}

	@Test
	public void testCourseGetForStudent() throws Exception {
		Course c = new Course("test get for student");
		Student st = new Student("CGFS");
		c.enrollStudent(st);

		daoFactory.getStudentDAO().persist(st);
		daoFactory.getCourseDAO().persist(c);

		List<Course> res = daoFactory.getCourseDAO().getForStudent(st);

		assertTrue(res.contains(c));
	}

	@AfterAll
	public static void cleanup() {
		daoFactory.close();
	}
}
