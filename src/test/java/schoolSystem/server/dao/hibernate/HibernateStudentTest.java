package schoolSystem.server.dao.hibernate;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.shared.domain.Student;

public class HibernateStudentTest {

	private static HibernateDAOFactory daoFactory;

	@BeforeAll
	public static void setup() {
		daoFactory = DAOFactoryProducer.newHibernateDAOFactory();
	}

	@Test
	public void testStudentGetByName() throws Exception {
		Student s = new Student("SGBN student");

		daoFactory.getStudentDAO().persist(s);

		List<Student> res = daoFactory.getStudentDAO().getByName(s.getName());

		assertTrue(res.contains(s));
	}

	@Test
	public void testSearch() throws Exception {
		final String BASE_NAME = LocalDateTime.now().toString();
		Student[] st = new Student[5];
		for(int i=0; i<5; i++) {
			st[i] = new Student(BASE_NAME+ i);
			daoFactory.getStudentDAO().persist(st[i]);
		}

		System.out.println("################################################");
		String phrase = BASE_NAME.substring(BASE_NAME.length()/2);
		System.out.println("SEARCH PHRASE: " + phrase);
		List<Student> res = daoFactory.getStudentDAO().search(phrase).stream().map(si -> si.item).collect(Collectors.toList());
		System.out.println(res);

		assertTrue(res.containsAll(Arrays.asList(st)));

		System.out.println("################################################");
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<BASE_NAME.length(); i+=2)
			sb.append(BASE_NAME.charAt(i));
		System.out.println("SEARCH PHRASE: " + sb);
		res = daoFactory.getStudentDAO().search(sb.toString()).stream().map(si -> si.item).collect(Collectors.toList());
		System.out.println(res);

		assertTrue(res.containsAll(Arrays.asList(st)));
	}

	@AfterAll
	public static void cleanup() {
		daoFactory.close();
	}
}
