package schoolSystem.server.dao.hibernate;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.shared.domain.Course;
import schoolSystem.shared.domain.Grade;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.Teacher;

public class HibernateGradeTest {

	private static HibernateDAOFactory daoFactory;

	@BeforeAll
	public static void setup() {
		daoFactory = DAOFactoryProducer.newHibernateDAOFactory();
	}

	@Test
	public void testGradeGetByStudent() throws Exception {
		Student st = new Student("get grade by student test");
		Teacher t = new Teacher("GGBS test");
		Course c = new Course("GGBS test");
		Grade g = new Grade(c, t, st, LocalDate.now(), 12);

		daoFactory.getStudentDAO().persist(st);
		daoFactory.getTeacherDAO().persist(t);
		daoFactory.getCourseDAO().persist(c);
		daoFactory.getGradeDAO().persist(g);

		List<Grade> res = daoFactory.getGradeDAO().getByStudent(st);

		assertTrue(res.contains(g));
	}

	@Test
	public void testGradeGetByCourse() throws Exception {
		Student st = new Student("GGBC test");
		Teacher t = new Teacher("GGBC test");
		Course c = new Course("GGBC test");
		Grade g = new Grade(c, t, st, LocalDate.now(), 12);

		daoFactory.getStudentDAO().persist(st);
		daoFactory.getTeacherDAO().persist(t);
		daoFactory.getCourseDAO().persist(c);
		daoFactory.getGradeDAO().persist(g);

		List<Grade> res = daoFactory.getGradeDAO().getByCourse(c);

		assertTrue(res.contains(g));
	}

	@Test
	public void testGradeGetForStudentByCourse() throws Exception {
		Student st = new Student("GGFS test");
		Teacher t = new Teacher("GGFS test");
		Course c = new Course("GGFS test");
		Grade g = new Grade(c, t, st, LocalDate.now(), 12);

		daoFactory.getStudentDAO().persist(st);
		daoFactory.getTeacherDAO().persist(t);
		daoFactory.getCourseDAO().persist(c);
		daoFactory.getGradeDAO().persist(g);

		List<Grade> res = daoFactory.getGradeDAO().getForStudentByCourse(st, c);

		assertTrue(res.contains(g));
	}

	@AfterAll
	public static void cleanup() {
		daoFactory.close();
	}
}
