package schoolSystem.server.dao.hibernate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.shared.domain.Password;
import schoolSystem.shared.domain.Student;

public class HibernatePasswordTest {

	private static HibernateDAOFactory daoFactory;

	@BeforeAll
	public static void setup() {
		daoFactory = DAOFactoryProducer.newHibernateDAOFactory();
	}

	@Test
	public void testPasswordGetForUser() throws Exception {
		Student u = new Student("PGFU test");
		Password p = new Password(u, "PGFU password");

		daoFactory.getStudentDAO().persist(u);
		daoFactory.getPasswordDAO().persist(p);

		assertEquals(p, daoFactory.getPasswordDAO().getForUser(u));
	}

	@Test
	public void testPasswordDeleteForUser() throws Exception {
		Student u = new Student("PDFU test");
		Password p = new Password(u, "PDFU password");

		daoFactory.getStudentDAO().persist(u);
		daoFactory.getPasswordDAO().persist(p);

		assertNotNull(p.getId());
		assertNotNull(daoFactory.getPasswordDAO().get(p.getId()));

		daoFactory.getPasswordDAO().deleteForUser(u);

		assertNull(daoFactory.getPasswordDAO().get(p.getId()));
	}

	@AfterAll
	public static void cleanup() {
		daoFactory.close();
	}
}
