package schoolSystem.server.dao.hibernate;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.shared.domain.Teacher;

public class HibernateTeacherTest {

	private static HibernateDAOFactory daoFactory;

	@BeforeAll
	public static void setup() {
		daoFactory = DAOFactoryProducer.newHibernateDAOFactory();
	}

	@Test
	public void testTeacherGetByName() throws Exception {
		Teacher t = new Teacher("TGBN teacher");

		daoFactory.getTeacherDAO().persist(t);

		List<Teacher> res = daoFactory.getTeacherDAO().getByName(t.getName());

		assertTrue(res.contains(t));
	}

	@AfterAll
	public static void cleanup() {
		daoFactory.close();
	}
}
