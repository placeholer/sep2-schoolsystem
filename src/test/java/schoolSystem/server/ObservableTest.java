package schoolSystem.server;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import schoolSystem.client.Client;
import schoolSystem.client.RmiClientImpl;
import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.server.dao.hibernate.HibernateDAOFactory;
import schoolSystem.server.security.AuthManager;
import schoolSystem.shared.mediator.ObservableMediator;
import schoolSystem.shared.mediator.SchoolSystemMediator;
import schoolSystem.shared.mediator.SchoolSystemMediatorImpl;
import schoolSystem.shared.util.Event;
import schoolSystem.shared.util.Listener;

public class ObservableTest {
	private static HibernateDAOFactory factory;
	private static RmiServerImpl server;

	private static final String HOST = "localhost";
	private static final int PORT = 1099;
	private static final String USERNAME = "test";
	private static final String PASSWORD = "test";

	@BeforeAll
	public static void initializeFactory() throws Exception {
		factory = DAOFactoryProducer.newHibernateDAOFactory();
		AuthManager auth = new AuthManager(factory);
		SchoolSystemMediator mediator = new SchoolSystemMediatorImpl(factory);
		mediator.addTeacher(USERNAME, PASSWORD);
		server = new RmiServerImpl(mediator, auth);
		server.start();
	}

	@Test
	public void listenersTest() throws Exception {
		Client c = new RmiClientImpl(HOST, PORT);
		c.login(USERNAME, PASSWORD);

		ObservableMediator mediator = c.getMediator();

		AtomicBoolean check = new AtomicBoolean(false);
		mediator.addListener(new Listener() {
			public <T> void handle(Event<T> e) {
				System.out.println("################################################");
				System.out.println(e);
				check.set(true);
			}
		});
		mediator.addStudent("listener test student", "password");

		assertTrue(check.get());

		c.disconnect();
	}

	@AfterAll
	public static void close() {
		server.close();
		factory.close();
	}
}
