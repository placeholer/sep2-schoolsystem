package schoolSystem.server.security;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import schoolSystem.client.Client;
import schoolSystem.client.ConnectionException;
import schoolSystem.client.RmiClient;
import schoolSystem.client.RmiClientImpl;
import schoolSystem.server.RmiServerImpl;
import schoolSystem.server.SchoolSystemServer;
import schoolSystem.server.dao.AbstractDAOFactory;
import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.shared.mediator.ObservableRemoteMediatorAdapter;
import schoolSystem.shared.mediator.SchoolSystemMediator;
import schoolSystem.shared.mediator.SchoolSystemMediatorImpl;
import schoolSystem.shared.security.InvalidTokenException;
import schoolSystem.shared.security.InvalidUsernameException;

public class ClientServerTest {

	private static SchoolSystemServer server;
	private static AbstractDAOFactory daoFactory;

	private static final String USERNAME = "test";
	private static final String PASSWORD = "test";

	private static final String HOST = "localhost";
	private static final int PORT = 1099;

	@BeforeAll
	public static void setupServer() {
		daoFactory = DAOFactoryProducer.newHibernateDAOFactory();

		SchoolSystemMediator mediator = new SchoolSystemMediatorImpl(daoFactory);
		mediator.addStudent(USERNAME, PASSWORD);

		AuthManager authManager = new AuthManager(daoFactory);

		server = new RmiServerImpl(mediator, authManager);
		server.start();
	}

	@Test
	public void testClientLogin() throws Exception {
		assertTimeoutPreemptively(Duration.ofSeconds(20), () -> {
			Client c = new RmiClientImpl(HOST, PORT);

			assertThrows(InvalidUsernameException.class, () -> c.login("INCORRECT USERNAME", "INCORRECT PASSWORD"));

			c.login(USERNAME, PASSWORD);
		}, "Connection timeout exceeded");
	}

	@Test
	public void testClientGetMediator() throws Exception {
		assertTimeoutPreemptively(Duration.ofSeconds(20), () -> {
			RmiClient c = new RmiClientImpl(HOST, PORT);

			assertThrows(IllegalStateException.class, () -> {
				c.getMediator();
			});

			c.login(USERNAME, PASSWORD);

			assertNotNull(c.getMediator());

			c.getMediator().addStudent("CGM TEST", "CGM TEST PASSWORD");

			c.disconnect();

			RmiClient c2 = new RmiClientImpl(HOST, PORT);
			c2.login("CGM TEST", "CGM TEST PASSWORD");
			c2.disconnect();
		});
	}

	@Test
	public void testTokenAuth() throws Exception {
		assertTimeoutPreemptively(Duration.ofSeconds(20), () -> {
			RmiClient c = new RmiClientImpl(HOST, PORT);
			c.login(USERNAME, PASSWORD);
			ObservableRemoteMediatorAdapter mediator = c.getRemoteAdapter();

			assertThrows(InvalidTokenException.class, () -> {
				mediator.getAllTeachers(UUID.randomUUID());
			});
		});
	}

	@Test
	public void testMultipleClients() throws Exception {
		assertTimeoutPreemptively(Duration.ofSeconds(30), () -> {
			RmiClient cl = new RmiClientImpl(HOST, PORT);
			cl.login(USERNAME, PASSWORD);
			ObservableRemoteMediatorAdapter med = cl.getRemoteAdapter();

			List<RmiClient> clients = new ArrayList<>(10);
			for(int i=0; i<10; i++) {
				med.addStudent(cl.getToken(), USERNAME + i, PASSWORD + i);

				RmiClient tmp = new RmiClientImpl(HOST, PORT);
				tmp.login(USERNAME + i, PASSWORD + i);
				clients.add(tmp);

				tmp.getRemoteAdapter().testToken(tmp.getToken());
			}

			cl.disconnect();

			clients.forEach(c -> {
				try {
					c.disconnect();
				} catch (ConnectionException e) {
					throw new RuntimeException(e);
				}
			});
		});
	}

	@AfterAll
	public static void cleanup() {
		if(null != server)
			server.close();
	}
}
