package schoolSystem;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import schoolSystem.shared.domain.Password;
import schoolSystem.shared.domain.Student;
import schoolSystem.shared.domain.User;

public class ModelsTest {

	@Test
	public void testPassword() throws Exception {
		User u = new Student("Test user");
		Password p = new Password(u, "test password");

		assertTrue(p.auth("test password"));
	}

}
