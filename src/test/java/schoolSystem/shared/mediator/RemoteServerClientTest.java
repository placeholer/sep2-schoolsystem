package schoolSystem.shared.mediator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import schoolSystem.client.RmiClient;
import schoolSystem.client.RmiClientImpl;
import schoolSystem.server.RmiServerImpl;
import schoolSystem.server.SchoolSystemServer;
import schoolSystem.server.dao.DAOFactoryProducer;
import schoolSystem.server.dao.hibernate.HibernateDAOFactory;
import schoolSystem.server.security.AuthManager;

public class RemoteServerClientTest {

	private static final String HOST = "localhost";
	private static final int PORT = 1099;

	private static final String USERNAME = "test";
	private static final String PASSWORD = "test";


	private static SchoolSystemServer server;
	private static HibernateDAOFactory factory;

	@BeforeAll
	public static void serverInit() {
		factory = DAOFactoryProducer.newHibernateDAOFactory();
		SchoolSystemMediator mediator = new SchoolSystemMediatorImpl(factory);
		mediator.addStudent(USERNAME, PASSWORD);
		server = new RmiServerImpl(mediator, new AuthManager(factory));
		server.start();
	}

	@Test
	public void testGetMediator() throws Exception {
		RmiClient client = new RmiClientImpl(HOST, PORT);

		assertThrows(IllegalStateException.class, () -> client.getMediator());

		client.login(USERNAME, PASSWORD);
		SchoolSystemMediator mediator = client.getMediator();

		assertNotNull(mediator);

		client.disconnect();
	}

	@Test
	public void testAddUser() throws Exception {
		RmiClient client = new RmiClientImpl(HOST, PORT);
		client.login(USERNAME, PASSWORD);
		SchoolSystemMediator mediator = client.getMediator();

		mediator.addStudent("new student", "password");

		client.disconnect();

		client = new RmiClientImpl(HOST, PORT);
		client.login("new student", "password");
		client.disconnect();
	}

	@Test
	public void testCheckConnection() throws Exception {
		RmiClient client = new RmiClientImpl(HOST, PORT);
		assertFalse(client.isLoggedIn());
		client.login(USERNAME, PASSWORD);

		assertTrue(client.isLoggedIn());

		client.disconnect();

		assertFalse(client.isConnected());
	}

	@AfterAll
	public static void serverClose() {
		server.close();
		factory.close();
	}
}
